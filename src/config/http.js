import axios from "axios";
import { DefaultUrlApi } from "./Configuration";

export const defaultResponseTransformers = () => {
  const { transformResponse } = axios.defaults;
  if (!transformResponse) return [];
  else if (transformResponse instanceof Array) return transformResponse;
  else return [transformResponse];
};

const http = axios.create({
  headers: {
    "Content-Type": "application/json",
  },

  baseURL: DefaultUrlApi,
  timeout: 10000,
});

http.interceptors.response.use(function (response) {
  return response;
});

export default http;
