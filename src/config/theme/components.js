import MuiButton from "./components/Button";
import MuiContainer from "./components/Container";
import MuiDrawer from "./components/Drawer";
import MuiTextField from "./components/TextField";

const components = {
  MuiButton,
  MuiTextField,
  MuiContainer,
  MuiDrawer,
};

export default components;
