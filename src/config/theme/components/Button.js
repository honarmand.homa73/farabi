const MuiButton = {
  styleOverrides: {
    root: ({ theme }) => ({
      "&.MuiButton-sizeMedium": {
        minHeight: "50px",
      },
      "&.MuiButton-sizeSmall": {
        minHeight: "34px",
        fontSize: "10px",
        minWidth: "72px",
      },
      borderRadius: "5px",
      textTransform: "none",
      letterSpacing: "0",

      "&.Mui-disabled": {
        opacity: 0.3,
        color: `${theme.palette.text.main}`,
      },
    }),
  },

  defaultProps: {
    variant: "contained",
    disableElevation: true,
  },
};

export default MuiButton;
