export const MuiDrawer = {
  styleOverrides: {
    root: {
      zIndex: 1600,
    },
  },
};

export default MuiDrawer;
