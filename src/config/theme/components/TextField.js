import palette from "../palette";

const MuiTextField = {
  styleOverrides: {
    root: {
      width: "100%",

      ".MuiInputBase-root": {
        backgroundColor: palette.modules.input,
        border: `1px solid ${palette.text.secondary}47`,
        borderRadius: "8px",
        paddingLeft: 0,

        ".MuiInputBase-input": {
          textAlign: "inherit",
          padding: "12px",
          "&::placeholder ": {
            fontSize: "12px",
          },
        },

        color: palette.text.primary,
      },

      ".MuiFormHelperText-root": {
        textAlign: "inherit",
        direction: "rtl",
      },

      fieldset: {
        border: "none !important",
      },
    },
  },

  // defaultProps: {
  //   dir: "rtl",
  // },
};

export default MuiTextField;
