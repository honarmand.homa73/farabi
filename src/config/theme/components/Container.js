const MuiContainer = {
  styleOverrides: {
    root: ({ theme }) => ({
      paddingRight: "16px",
      paddingLeft: "16px",
      [theme.breakpoints.up("md")]: {
        paddingRight: "40px !important",
        paddingLeft: "40px !important",
        maxWidth: "100%",
      },
    }),
  },
};

export default MuiContainer;
