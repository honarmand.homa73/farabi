const palette = {
  modules: {
    buttonEnd: "rgba(246, 241, 226, 0.5)",
    buttonBuy: "#F8F8F8",
    border: "#E6E6E6",
  },

  divider: "rgba(250, 250, 250, 0.1)",

  primary: {
    main: "#08874D",
    secondary: "#967A2F",
    alternate: "#F8F8F8",
  },

  secondary: {
    main: "#71d0f4",
    light: "#71d0f41a",
  },

  info: {
    main: "#364254",
  },

  text: {
    main: "#ffffff",
    primary: "#000",
    secondary: "#5C5C5C",
    alternate: "#ADADAD",
  },

  background: {
    default: "#f2f2f2",
    paper: "#ffffff",
    menu: "#29384f",
    disabled: "#364254",
    button: "#08874D",
    footer: "#265841",
    about: "rgba(255, 255, 255, 0.77)",
  },

  success: {
    main: "#08874D",
    primary: "#27a17c",
    light: "#27a17c",
  },

  error: {
    main: "#CF3318",
  },

  common: {
    white: "#ffffff",
    black: "#000000",
  },

  grey: {
    30: "#ddd",
    50: "#939dac",
    100: "#717781",
  },
};

export default palette;
