export const typography = {
  subtitle1: {
    fontSize: "12px",
    fontWeight: 400,
  },

  subtitle2: {
    fontSize: "11px",
    fontWeight: 400,
  },

  label: {
    fontSize: "13px",
    fontWeight: 500,
    display: "block",
    marginBottom: "4px",
    marginLeft: "8px",
  },
  text: {
    fontSize: "15px",
  },
  h1: {
    fontSize: "32px",
  },

  h2: {
    fontSize: "30px",
  },

  h3: {
    fontSize: "24px",
  },

  h4: {
    fontSize: "19px",
  },

  h5: {
    fontSize: "16px",
  },

  h6: {
    fontSize: "14px",
  },
};

export default typography;
