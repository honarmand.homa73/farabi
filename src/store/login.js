import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  codeOtp: "",
  mobileNumber: "",
  activeReserve: false,
  trackingCode: "",
  index: 1,
};

const stetSlice = createSlice({
  name: "login",
  initialState,
  reducers: {
    setCodeOtp(state, { payload }) {
      state.codeOtp = payload;
    },
    setMobileNumber(state, { payload }) {
      state.mobileNumber = payload;
    },
    setActiveReserve(state, { payload }) {
      state.activeReserve = payload;
    },
    setTrackingCode(state, { payload }) {
      state.trackingCode = payload;
    },
    setIndex(state, { payload }) {
      state.index = payload;
    },
  },
});

export const {
  setCodeOtp,
  setMobileNumber,
  setActiveReserve,
  setTrackingCode,
  setIndex,
} = stetSlice.actions;
export default stetSlice.reducer;
