import { createSlice } from "@reduxjs/toolkit";

const initialState = { information: {}, user: {}, recordTimeReservation: {} };

const stetSlice = createSlice({
  name: "informationUser",
  initialState,
  reducers: {
    setInformationUser(state, { payload }) {
      state.information = payload;
    },
    setUser(state, { payload }) {
      state.user = payload;
    },
    setRecordTimeReservation(state, { payload }) {
      state.recordTimeReservation = payload;
    },
  },
});

export const { setInformationUser, setUser, setRecordTimeReservation } =
  stetSlice.actions;
export default stetSlice.reducer;
