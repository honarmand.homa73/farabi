import { configureStore } from "@reduxjs/toolkit";
import step from "./step";
import selectedCoins from "./selectCoin";
import login from "./login";
import informationUser from "./informationUser";

export const store = configureStore({
  reducer: { selectedCoins, step, login, informationUser },

  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});
