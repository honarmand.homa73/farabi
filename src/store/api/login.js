export const CreateActivationCodeByMobileNumber = ({ mobileNumber }) =>
  `User/CreateActivationCodeByMobileNumber?mobile=${mobileNumber}`;

export const CheckActivationCodeByMobileNumber = ({ mobileNumber, codeOtp }) =>
  `User/CheckActivationCodeByMobileNumber?mobile=${mobileNumber}&code=${codeOtp}`;

export const GetActiveReserveByMobile = ({ mobileNumber }) =>
  `Reservation/GetActiveReserveByMobile?reservationId=01&mobile=${mobileNumber}`;
