export const GetLoginActivityList = `Reservation/CreateResrvation`;

export const GetDaysStatus = ({ date }) =>
  `Reservation/GetDaysStatus/01?duration=10`;

export const GetIntervalStatusByDay = ({ selectDateReservation }) =>
  `Reservation/GetIntervalStatusByDay/01?date=${selectDateReservation}`;

export const CancelReservation = ({ idReservation }) =>
  `Reservation/CancelReservation?reservationId=${idReservation}`;

export const GetCurrencyLists = `/Reservation/GetCurrencyLists`;
