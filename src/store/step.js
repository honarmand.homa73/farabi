import { createSlice } from "@reduxjs/toolkit";

const initialState = { step: "" };

const stetSlice = createSlice({
  name: "steps",
  initialState,
  reducers: {
    openModal(state, { payload }) {
      state.step = payload;
    },
    closeModal(state) {
      state.step = "";
    },
  },
});

export const { openModal, closeModal } = stetSlice.actions;
export default stetSlice.reducer;
