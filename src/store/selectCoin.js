import { createSlice } from "@reduxjs/toolkit";

const initialState = { selectedCoin: {} };

const stetSlice = createSlice({
  name: "selectedCoins",
  initialState,
  reducers: {
    setSelectedCoin(state, { payload }) {
      state.selectedCoin = payload;
    },
  },
});

export const { setSelectedCoin } = stetSlice.actions;
export default stetSlice.reducer;
