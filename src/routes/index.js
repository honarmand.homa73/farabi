import DesktopLayout from "../layouts/Desktop";
import AboutUs from "../pages/AboutUs";
import Home from "../pages/Home";
import OurServices from "../pages/OurServices";

const routes = () => {
  return [
    {
      element: <DesktopLayout />,
      order: 0,
      children: [
        { path: "/", element: <Home /> },
        { path: "/our-service", element: <OurServices /> },
        { path: "/about-us", element: <AboutUs /> },
      ],
    },

    {
      path: "*",
      element: <Home />,
      order: 0,
    },
  ];
};

export default routes;
