import { Box, Container, Grid, styled, Typography } from "@mui/material";
import BackgroundService from "../../assets/images/BackgroundService.jpg";
import BoxService from "./Component/BoxService";
import Banknote from "../../assets/images/services/Banknote.png";
import Cash from "../../assets/images/services/Cash.png";
import Coin from "../../assets/images/services/Coin.png";
import Remittance from "../../assets/images/services/Remittance.png";

function OurServices() {
  return (
    <BoxStyle Background={BackgroundService}>
      <Container maxWidth="lg">
        <Grid container direction="row-reverse" spacing={0.5}>
          <Grid item xs={12}>
            <BoxWith>
              <BoxText pb={3}>
                <Typography
                  textAlign="right"
                  variant="h3"
                  fontWeight={800}
                  pt={8}
                  color="text.main"
                >
                  خدمات صرافی فارابی
                </Typography>
                <Typography
                  color="text.main"
                  pt={1.5}
                  variant="h6"
                  textAlign="right"
                >
                  کامل‌ترین خدمات صرافی، خرید و فروش ارز، خرید و فروش سکه و شمش
                  طلا
                </Typography>
              </BoxText>

              <BoxWrapper>
                <BoxService
                  status={"Banknote"}
                  gridColumn={"6/11"}
                  gridRow={"3"}
                  picture={Banknote}
                  title={"خرید و فروش اسکناس "}
                  text={
                    "شرکت صرافی رحمان بابازاده و شرکاء به امر تامین ارز از طریق بازار متشکل ارزی و یا مشتریان اقدام و سپس براساس کارمزدهای مصوب بانک مرکزی ج.ا.ا اقدام به فروش و عرضه ارزهای موجود می نماید . در حال حاضر این صرافی در زمینه خرید و فروش اسکناس های یورو ؛ دلار و درهم فعال می باشد."
                  }
                />

                <BoxService
                  status={"Remittance"}
                  gridColumn={"1/7"}
                  gridRow={"6"}
                  picture={Remittance}
                  title={"خرید و فروش حواله"}
                  text={
                    "شرکت صرافی رحمان بابازاده و شرکاء در چارچوب قوانین و مقررات ارزی بانک مرکزی ج.ا.ا نسبت به تامین ارز از طریق سامانه های نیما و سنا اقدام و به واردکنندگان مجاز دارای گواهی ثبت آماری در کلیه گروه های کالایی ارائه می نماید . از سوی دیگر به منظور تسهیل تبدیلات ؛ نقل و انتقالات ارزی در چارچوب مقررات همواره سعی بر آن دارد تا با برقراری ارتباطات موثر و دوسویه با کارگزاران معتبر و قابل اطمینان در این عرصه ، حوزه فعالیت شرکت را گسترش دهد .در حال حاضر این صرافی دارای کارگزاران معتبر در اروپا ، چین ، هنگ کنگ، امارات متحده عربی، ترکیه و … می باشد و به صورت نامحدود امکان دریافت و پرداخت ارز وجود خواهد داشت ."
                  }
                />

                <BoxService
                  status={"Cash"}
                  gridColumn={"7/12"}
                  gridRow={"10"}
                  picture={Cash}
                  title={"خدمات کارگزاری و مدیریت وجوه نقد در خارج از کشور"}
                  text={
                    "این صرافی با اعتبارسنجی و ارتباط با کارگزاران فعال و قابل اعتماد در اروپا ، ترکیه ، امارات متحده عربی ، چین و هنگ کنگ می تواند نسبت به دریافت و مدیریت وجوه ارزی صادرکنندگان اقدام نماید. این موضوع می تواند گروه گشای مشکلات چگونگی دریافت و پرداخت ارزی تجار و صادرکنندگان و بازرگانان ایرانی که قصد صادرات به کشورهای خارجی را داشته ولی بدلیل مشکلات انتقالات و مبادلات ارزی امکان دریافت این مبلغ را ندارند گردد."
                  }
                />

                <BoxShadow />

                <BoxService
                  status={"Coin"}
                  gridColumn={"3/7"}
                  gridRow={"14"}
                  picture={Coin}
                  title={`خرید و فروش سکه، مسکوکات، 
                  شمش و سکه یادبود`}
                />
              </BoxWrapper>
            </BoxWith>
          </Grid>
        </Grid>
      </Container>
    </BoxStyle>
  );
}

const BoxWith = styled(Box)(() => ({
  maxWidth: "1065px",
  margin: "auto",
}));

const BoxText = styled(Box)(({ theme }) => ({
  position: "relative",
  marginBottom: theme.spacing(3),

  [theme.breakpoints.up("lg")]: {
    top: "130px",
    width: "354px",
    marginLeft: "auto",
    marginBottom: "0",
  },
}));

const BoxStyle = styled(Box)(({ theme, Background }) => ({
  backgroundColor: theme.palette.background.default,
  backgroundImage: `url(${Background})`,
  overflow: "hidden",
  backgroundRepeat: "no-repeat",
  backgroundPosition: "center",
  backgroundSize: "cover",
  [theme.breakpoints.down("lg")]: {
    height: "auto",
  },
  paddingBottom: theme.spacing(2),
}));

const BoxWrapper = styled(Box)(({ theme }) => ({
  position: "relative",
  gap: "10px",

  [theme.breakpoints.up("lg")]: {
    display: "grid",
    gridTemplateColumns: "repeat(12,1fr)",
    gridTemplateRows: "repeat(18,70px)",
  },
}));

const BoxShadow = styled(Box)(({ theme }) => ({
  position: "absolute",
  width: "571.44px",
  height: "478.64px",
  left: "calc(13% - 1071.44px/2 + 808.76px)",
  top: "calc(22% - 500.64px/2 + 676.83px)",
  background:
    "linear-gradient(179.18deg, rgba(157, 235, 200, 0.35) 0.7%, rgba(157, 235, 200, 0) 76.84%)",
  background:
    "linear-gradient(179.18deg, rgba(157, 235, 200, 0.35) 0.7%, rgba(157, 235, 200, 0) 76.84%)",
  transform: "rotate(135deg)",
}));

export default OurServices;
