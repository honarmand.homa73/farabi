import { Box, styled, Typography } from "@mui/material";

export default function BoxService({
  picture,
  title,
  text,
  gridColumn,
  gridRow,
  status,
}) {
  return (
    <BoxStyle gridColumn={gridColumn} gridRow={gridRow} status={status}>
      <BoxFlex status={status}>
        <img src={picture} alt="" />
        <Typography variant="h4" fontWeight={800} py={3} lineHeight={1.7}>
          {" "}
          {title}
        </Typography>
        <Typography variant="h6" textAlign="justify" lineHeight={1.7}>
          {text}
        </Typography>
      </BoxFlex>
    </BoxStyle>
  );
}

const BoxStyle = styled(Box)(({ theme, gridColumn, gridRow, status }) => ({
  position: "relative ",
  gridColumn: gridColumn,
  gridRow: gridRow,
  zIndex: 2,
  [theme.breakpoints.down("lg")]: {
    background: "#FAFAFA",
    marginBottom: theme.spacing(2),
    borderRadius: "12px",
    padding: "32px 24px",
    display: "flex",
  },
  [theme.breakpoints.up("lg")]: {
    "&:before": {
      content: "''",
      position: "absolute",
      background: "#FAFAFA",
      width:
        (status === "Banknote" && "430px") ||
        (status === "Remittance" && "530px") ||
        (status === "Cash" && "517px") ||
        (status === "Coin" && "285px"),

      height:
        (status === "Banknote" && "430px") ||
        (status === "Remittance" && "530px") ||
        (status === "Cash" && "517px") ||
        (status === "Coin" && "285px"),

      left:
        (status === "Banknote" && "0") ||
        (status === "Remittance" && "16px") ||
        (status === "Cash" && "-44px") ||
        (status === "Coin" && "59px"),
      top:
        (status === "Banknote" && "0") ||
        (status === "Remittance" && "19px") ||
        (status === "Cash" && "-14px") ||
        (status === "Coin" && "-12px"),
      transform: "rotate(135deg)",
      zIndex: -1,
    },

    "&:after": {
      content: "''",
      position: "absolute",
      width:
        (status === "Banknote" && "899.82px") ||
        (status === "Remittance" && "794.2px") ||
        (status === "Cash" && "714.2px") ||
        (status === "Coin" && "342.44px"),
      height:
        (status === "Banknote" && "562.73px") ||
        (status === "Remittance" && "794.2px") ||
        (status === "Cash" && "714.2px") ||
        (status === "Coin" && "342.44px"),
      left:
        (status === "Banknote" && "calc(16% - 992.82px/2)") ||
        (status === "Remittance" && "calc(-35% - 992.82px/2)") ||
        (status === "Cash" && "calc(135% - 750.14px/2 + 150.01px)") ||
        (status === "Coin" && "calc(-22% - 649.44px/2 + 343.76px)"),

      top:
        (status === "Banknote" && "calc(48% - 623.73px/1)") ||
        (status === "Remittance" && "calc(4% - 794.2px/2 + 659.18px)") ||
        (status === "Cash" && "calc(104% - 714.14px/2 + -198.46px)") ||
        (status === "Coin" && "calc(-47% - 136.64px/2 + 17.83px)"),

      background:
        "linear-gradient(179.18deg, rgba(157, 235, 200, 0.35) 0.7%, rgba(157, 235, 200, 0) 76.84%)",
      transform: status === "Cash" ? "rotate(315deg)" : "rotate(135deg)",
      zIndex: "-2",
    },
  },
}));

const BoxFlex = styled(Box)(({ status, theme }) => ({
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  flexDirection: "column",
  padding: status === "Remittance" ? "20px 85px" : "15px",
  direction: "rtl",
  [theme.breakpoints.down("lg")]: {
    alignItems: "start",
    padding: 0,
    width: "100%",
  },
  img: {
    width: "80px",
    height: "80px",
  },
}));
