import { Box, Container, Grid, styled, Typography } from "@mui/material";
import About from "../../assets/images/AboutUs.jpg";
import AboutAddress from "../../assets/images/AboutAddress.jpg";
import Logo from "../../assets/logo/farabi.svg";

function AboutUs() {
  return (
    <>
      <BoxStyle Background={About}>
        <Container maxWidth="lg">
          <Grid container direction="row-reverse" spacing={0.5}>
            <Grid item lg={7} xs={12}>
              <Typography variant="h1" dir="rtl" fontWeight={800} pb={3}>
                درباره صرافی فارابی
              </Typography>

              <BoxText>
                <Typography variant="text" dir="rtl" lineHeight={2}>
                  شرکت صرافی “رحمان بابازاده و شرکاء” با اخذ مجوز از بانک مرکزی
                  جمهوری اسلامی ایران در راستای ارائه خدمات ارزی در زمینه های
                  زیر به صادر کنندگان و واردکنندگان کشور می پردازد:
                </Typography>

                <BoxList>
                  <Typography variant="text" dir="rtl">
                    • تامین ارز بابت کلیه گروه های کالایی از طریق سامانه نیما از
                    صادرکنندگان محترم و واگذاری و فروش مستقیم ارز در سامانه نیما
                    به وارد کنندگان دارای گواهی های ثبت آماری تایید شده.
                  </Typography>

                  <Typography variant="text" dir="rtl">
                    • تبدیل ارز بابت کلیه گروه های کالایی و تمامی ارزهای رایج
                    کشور
                  </Typography>

                  <Typography variant="text" dir="rtl">
                    • مدیریت و نگهداری وجوه ارزی در خارج از کشور (اروپا ـ چین ـ
                    هنگ کنگ ـ امارات متحده عربی)
                  </Typography>

                  <Typography variant="text" dir="rtl">
                    • تامین و انتقال ارز از طریق سامانه سنا
                  </Typography>

                  <Typography variant="text" dir="rtl">
                    • خرید و فروش ارز به صورت اسکناس از طریق بازار متشکل ارزی و
                    یا مراجعه حضوری
                  </Typography>

                  <Typography variant="text" dir="rtl">
                    • خرید و فروش سکه
                  </Typography>
                </BoxList>
              </BoxText>
            </Grid>
          </Grid>
        </Container>
      </BoxStyle>

      <BoxAddress Background={AboutAddress}>
        <Container maxWidth="lg">
          <BoxContainer>
            <Grid container direction="row-reverse" spacing={0.5}>
              <Grid
                item
                lg={5}
                xs={12}
                pl={{ md: "20px !important", sm: " 0" }}
              >
                <img className="main-logo" src={Logo} alt="" />

                <Box textAlign="right">
                  <Typography variant="h5" fontWeight={700} mt={3}>
                    آدرس
                  </Typography>

                  <Typography variant="text" mt={0.5} fontWeight={400}>
                    شهرک قدس(غرب) ،بلوار خوردین پایین تر تقاطع دریا، نبش توحید
                    سوم، پلاک ۳، طبقه اول
                  </Typography>
                </Box>

                <Box textAlign="right">
                  <Typography variant="h5" fontWeight={700} mt={3}>
                    تلفن
                  </Typography>

                  <Typography variant="h6" mt={0.5} fontWeight={400}>
                    ۰۲۱ - ۲۱۵۶۱۷۷۷
                  </Typography>
                </Box>
              </Grid>

              <Grid item lg={7} xs={12}>
                <BoxMap>
                  <iframe
                    class="map"
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d808.9929257501344!2d51.43357682919819!3d35.800627998760426!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1b3eaa0ae3e2dc3e!2zMzXCsDQ4JzAyLjMiTiA1McKwMjYnMDIuOSJF!5e0!3m2!1sen!2sde!4v1654521605401!5m2!1sen!2sde"
                    width="600"
                    height="450"
                    allowfullscreen=""
                    loading="lazy"
                    referrerpolicy="no-referrer-when-downgrade"
                  ></iframe>
                </BoxMap>
              </Grid>
            </Grid>
          </BoxContainer>
        </Container>
      </BoxAddress>
    </>
  );
}

const BoxStyle = styled(Box)(({ theme, Background }) => ({
  backgroundImage: `url(${Background})`,
  position: "relative",
  backgroundRepeat: "no-repeat",
  backgroundPosition: "center",
  backgroundSize: "cover",
  paddingBottom: theme.spacing(20),
  paddingTop: theme.spacing(20),
  backgroundPositionX: "left",
  [theme.breakpoints.down("md")]: {
    paddingBottom: theme.spacing(7),
    paddingTop: theme.spacing(7),
  },
}));

const BoxText = styled(Box)(({ theme }) => ({
  display: "flex",
  flexDirection: "column",
  justifyContent: "flex-end",
  alignItems: "flex-end",
  padding: "24px 24px 32px 150px",
  gap: "10px",
  background: theme.palette.background.about,
  backdropFilter: "blur(6px)",
  borderRadius: "12px",
  [theme.breakpoints.down("md")]: {
    padding: theme.spacing(3),
  },
}));

const BoxAddress = styled(Box)(({ theme, Background }) => ({
  backgroundImage: `url(${Background})`,
  position: "relative",
  backgroundRepeat: "no-repeat",
  backgroundPosition: "center",
  backgroundSize: "cover",
  backgroundPositionX: "left",
  paddingBottom: theme.spacing(2),
}));

const BoxContainer = styled(Box)(({ theme }) => ({
  display: "flex",
  flexDirection: "row",
  justifyContent: "center",
  alignItems: "flex-start",
  padding: theme.spacing(6),
  background: theme.palette.background.paper,
  border: `1px solid ${theme.palette.modules.border}`,
  borderRadius: "12px",
  margin: "54px 200px",
  [theme.breakpoints.down("lg")]: {
    margin: theme.spacing(0),
    marginTop: theme.spacing(4),
    padding: theme.spacing(2),
  },
  img: {
    display: "flex",
    marginRight: "0",
    marginLeft: "auto",
  },
}));

const BoxMap = styled(Box)(({ theme }) => ({
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  height: "278px",

  [theme.breakpoints.down("lg")]: {
    marginTop: theme.spacing(2),
  },
  iframe: {
    width: "100%",
    height: "100%",
  },
}));

const BoxList = styled(Box)(({ theme }) => ({
  paddingRight: theme.spacing(2),
  display: "flex",
  flexDirection: "column",
  gap: 10,
  ".MuiTypography-root": {
    lineHeight: 2,
  },
}));

export default AboutUs;
