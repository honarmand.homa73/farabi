import { Box, Button, styled, Typography, useTheme } from "@mui/material";
import React, { useMemo } from "react";
import { Danger, Left } from "../../../assets/Icon";
import USD from "../../../assets/images/coin/USD.png";
import EUR from "../../../assets/images/coin/EUR.png";
import gold from "../../../assets/images/coin/gold.png";
import { STATUS_COIN, TYPE_COIN } from "../constants";
import { Price } from "../../../utils/helpers/Price";
import { useDispatch } from "react-redux";
import { openModal } from "../../../store/step";
import { setSelectedCoin } from "../../../store/selectCoin";
import { setMobileNumber } from "../../../store/login";

function BoxCoin({ coin, icon }) {
  const theme = useTheme();
  const dispatch = useDispatch();

  const handleClick = (coin) => {
    dispatch(setMobileNumber(""));
    dispatch(openModal("login"));
    dispatch(setSelectedCoin(coin));
  };

  const showCoin = useMemo(() => {
    if (coin?.Title === TYPE_COIN.USD) {
      return USD;
    } else if (coin?.Title === TYPE_COIN.EUR) {
      return EUR;
    } else if (coin?.Title === TYPE_COIN.COIN) {
      return gold;
    } else {
      return "DER";
    }
  }, [coin]);

  return (
    <BoxStyle>
      <Box
        display="flex"
        justifyContent="space-between "
        dir="rtl"
        width="100%"
      >
        <Box display="flex">
          <img
            src={icon !== undefined ? { icon } : showCoin}
            alt=""
            width="34px"
            height="24px"
          />

          <Typography variant="h5" fontWeight={600} pr={1}>
            {coin?.Title}
          </Typography>
          {/* <Typography variant="h5" color="text.alternate" px={0.5}>
            / USD
          </Typography> */}
        </Box>
        <Box>
          {/* <img src={Vector} alt="" width="60px" height="21px" /> */}
        </Box>
      </Box>

      <Box
        display="flex"
        justifyContent="space-between "
        dir="rtl"
        width="100%"
      >
        <Typography variant="h6" color="text.secondary">
          قیمت فروش (تومان)
        </Typography>

        <Typography variant="h4" fontWeight={600}>
          <Price number={coin?.SellPrice} />
        </Typography>
      </Box>

      <Box
        display="flex"
        justifyContent="space-between "
        dir="rtl"
        width="100%"
      >
        <Typography variant="h6" color="text.secondary">
          قیمت خرید (تومان)
        </Typography>

        <Typography variant="h4" fontWeight={600}>
          <Price number={coin?.PurchasePrice} />
        </Typography>
      </Box>

      <BoxButton colorStatus={coin?.Enable}>
        {coin?.Enable === STATUS_COIN.available && (
          <ButtonBuy
            variant="text"
            fullWidth
            onClick={() => handleClick(coin.Order)}
          >
            <Left color={theme.palette.background.button} />
            درخواست آنلاین نوبت خرید
          </ButtonBuy>
        )}
        {coin?.Enable === STATUS_COIN.inactive && (
          <Button
            variant="text"
            dir="rtl"
            fullWidth
            onClick={() => handleClick(coin.Order)}
          >
            <Typography color={theme.palette.text.secondary}>
              موقتا غیرفعال!
            </Typography>
          </Button>
        )}
        {coin?.Enable === STATUS_COIN.fullCapacity && (
          <ButtonEnd
            variant="text"
            fullWidth
            onClick={() => handleClick(coin.Order)}
            disabled={true}
          >
            <Danger color={theme.palette.primary.secondary} />
            <Typography pl={0.5}> اتمام ظرفیت نوبت‌دهی</Typography>
          </ButtonEnd>
        )}
      </BoxButton>
    </BoxStyle>
  );
}

const BoxStyle = styled(Box)(({ theme }) => ({
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
  padding: "24px",
  gap: "16px",
  background: "rgba(255, 255, 255, 0.9)",
  border: "1px solid #F0F0F0",
  backdropFilter: "blur(12px)",
  borderRadius: "12px",
}));

const BoxButton = styled(Button)(({ theme, colorStatus }) => ({
  display: "flex",
  flexDirection: "row",
  justifyContent: "center",
  alignItems: "center",
  gap: "6px",
  background:
    colorStatus === STATUS_COIN.fullCapacity
      ? theme.palette.modules.buttonEnd
      : theme.palette.modules.buttonBuy,
  borderRadius: "6px",
  flex: "none",
  order: "0",
  alignSelf: "stretch",
  flexGrow: "0",
  maxHeight: "48px",
  padding: 0,
  border: "1.5px solid #F0F0F0",
}));

const ButtonBuy = styled(Button)(({ theme }) => ({
  color: theme.palette.background.button,
  "&:hover": {
    color: theme.palette.text.main,
  },
}));

const ButtonEnd = styled(Button)(({ theme }) => ({
  color: theme.palette.primary.secondary,
  display: "flex",
  alignItems: "center",
  "&:hover": {
    color: theme.palette.text.main,
    backgroundColor: "snow",
  },
  "&.Mui-disabled": {
    opacity: 1,
    color: theme.palette.primary.secondary,
  },
}));

export default BoxCoin;
