export const STATUS_COIN = {
  available: true,
  inactive: false,
  // fullCapacity: 3,
};
export const TYPE_COIN = {
  USD: "دلار امریکا / USD",
  EUR: "یورو",
  DER: "درهم",
  COIN: "ربع سکه",
};
