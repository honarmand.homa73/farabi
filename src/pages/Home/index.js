import { Alert, Box, Container, Grid, styled, Typography } from "@mui/material";
import BackDesktop from "../../assets/logo/BackDesktop.png";
import BoxCoin from "./Component/BoxCoin";
import { GetCurrencyLists } from "../../store/api/createReservation";
import http from "../../config/http";
import { useEffect, useState } from "react";

function Home() {
  const [currencyLists, setCurrencyLists] = useState([]);
  const [textError, setTextError] = useState("");

  console.log("currencyLists", currencyLists);

  const getCurrencyLists = async () => {
    try {
      const { data } = await http.get(GetCurrencyLists);
      setCurrencyLists(data?.Result);
    } catch (e) {
      setTextError(e?.ErrorMessages[0]);
    }
  };

  useEffect(() => {
    getCurrencyLists();
  }, []);

  const sortedCurrencyLists = currencyLists
    .slice()
    .sort((a, b) => a.Order - b.Order);
  return (
    <>
      {textError && (
        <AlertStyle
          severity="error"
          sx={{
            direction: "rtl",
            width: "100%",
            position: "relative",
            zIndex: 10,
            top: "32px",
            ".MuiAlert-icon": {
              marginLeft: "5px",
            },
          }}
        >
          {textError}
        </AlertStyle>
      )}

      <BoxStyle BackDesktop={BackDesktop}>
        <Container maxWidth="lg">
          <Grid container direction="row-reverse" spacing={0.5}>
            <Grid item md={3} xs={12}>
              <BoxCenter>
                <Typography variant="h2" fontWeight={800} mb={4}>
                  صرافی فارابی
                </Typography>

                <Typography
                  lineHeight={2}
                  pl={{ md: 4, xs: 0 }}
                  variant="h6"
                  textAlign={{ md: "justify", xs: "center" }}
                  dir="rtl"
                  color="text.secondary"
                >
                  شرکت تضامنی بابازاده و شرکا انواع خدمات صرافی اعم از خرید و
                  فروش اسکناس را با بهترین کیفیت به مشتریان گرامی ارائه
                  می‌نماید.
                </Typography>
              </BoxCenter>
            </Grid>

            <Grid item md={9} xs={12} textAlign="right">
              <BoxCenter>
                <Grid container direction="row-reverse" spacing={2}>
                  {sortedCurrencyLists?.map((coin) => (
                    <Grid item lg={4} sm={6} xs={12} textAlign="right">
                      <BoxCoin coin={coin} icon={coin.Image} />
                    </Grid>
                  ))}
                </Grid>
              </BoxCenter>
            </Grid>
          </Grid>
        </Container>
      </BoxStyle>
    </>
  );
}

const BoxStyle = styled(Box)(({ theme, BackDesktop }) => ({
  backgroundColor: theme.palette.background.default,
  backgroundImage: `url(${BackDesktop})`,
  position: "relative",
  height: "80vh",
  backgroundRepeat: "no-repeat",
  backgroundPosition: "center",
  backgroundSize: "cover",
  [theme.breakpoints.down("lg")]: {
    height: "auto",
  },
  paddingBottom: theme.spacing(2),
}));

const BoxCenter = styled(Box)(({ theme }) => ({
  height: "80vh",
  display: "flex",
  flexDirection: "column",
  alignItems: "flex-end",
  justifyContent: "center",
  [theme.breakpoints.down("lg")]: {
    height: "auto",
    paddingTop: theme.spacing(3),
    alignItems: "center",
  },
}));

const AlertStyle = styled(Alert)(({ theme }) => ({
  [theme.breakpoints.down("md")]: {
    position: "fixed",
    top: 0,
    left: 0,
    right: 0,
  },
}));
export default Home;
