export function PersianNum(num) {
  const persianNumbers = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];

  const englishNumbers = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];

  const numString = num?.toString();
  let persianized = "";

  for (let i = 0; i < numString?.length; i++) {
    const char = numString.charAt(i);
    const index = englishNumbers.indexOf(char);
    persianized += index !== -1 ? persianNumbers[index] : char;
  }

  return persianized;
}
