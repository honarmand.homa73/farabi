import persianjs from "persianjs";
function thousandSeparator(number) {
  return number
    .toString()
    .replace(/\D/g, "")
    .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
export function Price({ number }) {
  if (
    number === null ||
    number === "" ||
    number === undefined ||
    isNaN(number) ||
    number === 0
  ) {
    return 0;
  }
  const numberSeparator = thousandSeparator(number);
  const value = persianjs(numberSeparator.toString())
    .englishNumber()
    .toString();
  return value;
}
