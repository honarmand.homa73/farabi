import * as yup from "yup";

const phoneRegExp = /^[0-9]*\.?[0-9]+$/;
// const identicalDigits = [
//   "0000000000",
//   "1111111111",
//   "2222222222",
//   "3333333333",
//   "4444444444",
//   "5555555555",
//   "6666666666",
//   "7777777777",
//   "8888888888",
//   "9999999999",
// ];

export function AddEditRegistration() {
  return yup
    .object({
      firstName: yup.string().required("نام خود را وارد کنید"),
      lastName: yup.string().required(" نام خانوادگی خود را وارد کنید"),
      dayTurnID: yup.string().required("ساعت مرجعه را انتخاب کنید"),
      nationalCode: yup
        .string()
        .test("is-unique", "شماره ملی صحیح نیست", (value) => {
          if (!value) {
            return true;
          }
          const firstDigit = value[0];
          return !value.split("").every((digit) => digit === firstDigit);
        })
        .required("شماره ملی خود وارد کنید")
        .matches(phoneRegExp, "لطفا عدد وارد کنید")

        // .matches(identicalDigits, "شماره ملی صحیح نیست")
        .min(10, "کد ملی باید 10 رقم باشد")
        .max(10, "کد ملی باید 10 رقم باشد"),

      // foreignCurrency: yup
      //   .string()
      //   .required("شماره حسال ارزی خود را وارد کنید")
      //   .matches(phoneRegExp, "لطفا عدد وارد کنید"),
    })
    .required();
}
