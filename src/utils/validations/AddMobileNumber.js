import * as yup from "yup";

export function AddMobileNumber() {
  return yup
    .object({
      mobileNumber: yup
        .number()
        .required("تنها مجاز به استفاده از عدد میباشید.")
        .min(4, "شماره همراه باید بیش از ۴ کاراکتر باشد.")
        .max(12, "شماره همراه نمی‌تواند بیش از ۱۲ کاراکتر باشد"),
    })
    .required();
}
