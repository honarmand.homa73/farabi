import { Box, Grid, styled } from "@mui/material";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Outlet as RouterOutlet } from "react-router-dom";
import ButtonCoin from "../components/ButtonCoin";
import Footer from "../components/Footer";
import Header from "../components/Header";
import Modal from "../components/Modal";
import { closeModal } from "../store/step";
import InsideModal from "../components/InsideModal";
import { TYPE_STEP } from "../components/constance";

export default function DesktopLayout() {
  const [OpenModalAuth, setOpenModalAuth] = useState(false);
  const openModal = useSelector((state) => state.step.step);

  const dispatch = useDispatch();

  const handleOnClose = () => {
    dispatch(closeModal());
  };

  return (
    <>
      <BoxMain>
        <Header
          OpenModalAuth={OpenModalAuth}
          setOpenModalAuth={setOpenModalAuth}
        />

        <RouterOutlet
          OpenModalAuth={OpenModalAuth}
          setOpenModalAuth={setOpenModalAuth}
        />

        <BoxBuy>
          <ButtonCoin />
        </BoxBuy>

        <Footer />
      </BoxMain>

      {/* modalAuth */}
      <Modal
        maxWidth={
          openModal === TYPE_STEP.Form_Modal ||
          openModal === TYPE_STEP.SHOW_REQUEST
            ? "sm"
            : "xs"
        }
        dir="rtl"
        open={openModal !== ""}
        onClose={handleOnClose}
      >
        <Grid item xs={12} textAlign="right">
          <InsideModal onClose={handleOnClose} />
        </Grid>
      </Modal>
    </>
  );
}

const BoxMain = styled(Box)(() => ({
  display: "flex",
  flexDirection: "column",
}));

const BoxBuy = styled(Box)(({ theme }) => ({
  display: "none",
  [theme.breakpoints.down("sm")]: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    padding: "12px 16px",
    gap: "12px",
    position: "fixed",
    width: "100%",
    bottom: "0",
    background: theme.palette.common.white,
    boxShadow: "0px -2px 4px rgba(0, 0, 0, 0.04)",
    zIndex: "5",
  },
}));
