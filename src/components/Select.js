import {
  IconButton,
  MenuItem,
  TextField,
  styled,
  useTheme,
} from "@mui/material";

import { useState } from "react";
import { Down } from "../assets/Icon";

function Select({
  disabled,
  value,
  onChange,
  items,
  itemText,
  itemValue,
  customValue,
  justifyContent,
  dir,
  placeholderText,
  marginTop,
  maxHeight,
  ...props
}) {
  const theme = useTheme();
  const [open, setOpen] = useState(false);

  return (
    <TextFieldSelect
      select
      disabled={disabled}
      value={value}
      onChange={onChange}
      dir={dir}
      SelectProps={{
        open: open,
        onOpen: () => setOpen(true),
        onClose: () => setOpen(false),
        IconComponent: (props) => {
          return (
            <IconButton size="small" onClick={() => setOpen(true)} {...props}>
              <Down color={theme.palette.text.secondary} />
            </IconButton>
          );
        },
        MenuProps: {
          sx: {
            zIndex: "1600",
            maxHeight: maxHeight || "300px",
            marginTop: marginTop || 1,
            minWidth: "300px",
            maxWidth: `600px !important`,
            direction: dir || "rtl",
          },
        },
      }}
      {...props}
    >
      {items?.map((option, index) => (
        <MenuItem
          sx={{
            justifyContent: justifyContent || "flex-start",
          }}
          key={index}
          value={option[itemValue] || option}
        >
          {customValue ? customValue(option) : option[itemText] || option}
        </MenuItem>
      ))}
    </TextFieldSelect>
  );
}

const TextFieldSelect = styled(TextField)(() => ({
  ".MuiButtonBase-root": {
    position: "absolute",
    top: "50%",
    right: "8px",
    transform: "translateY(-50%)",
    transition: "all 0.3s",
    "&.MuiSelect-iconOpen": {
      transform: "translateY(-50%) rotate(180deg)",
    },
  },

  '&[dir="rtl"]': {
    ".MuiSelect-select": {
      padding: "12.5px 14px",
      paddingRight: "14px !important",
      paddingLeft: "32px !important",
    },
    ".MuiButtonBase-root": {
      right: "initial",
      left: "8px",
    },
  },
}));

export default Select;
