import { NumericFormat } from "react-number-format";

export function NumberFormats({ displayType, value }) {
  return (
    <NumericFormat
      displayType={displayType || "text"}
      value={value}
      thousandSeparator={true}
    />
  );
}

export default NumberFormats;
