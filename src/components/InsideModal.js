import React from "react";
import { TYPE_STEP } from "./constance";
import AuthModal from "./ModalAll/AuthModal";
import OtpModal from "./ModalAll/OtpModal";
import { useSelector } from "react-redux";
import SelectionCoinModal from "./ModalAll/SelectionCoinModal";
import CancellationConfirmation from "./ModalAll/CancellationConfirmation";
import RegistrationSuccessModal from "./ModalAll/RegistrationSuccessModal";
import Error from "./ModalAll/Error";
import FormModal from "./ModalAll/FormModal";
import DissuasionModal from "./ModalAll/DissuasionModal";
import ShowRequest from "./ModalAll/ShowRequest";

export default function InsideModal({ onClose }) {
  const step = useSelector((state) => state.step.step);

  if (step === TYPE_STEP.LOGIN) {
    return <AuthModal />;
  } else if (step === TYPE_STEP.OTP) {
    return <OtpModal />;
  } else if (step === TYPE_STEP.SELECT_COIN) {
    return <SelectionCoinModal />;
  } else if (step === TYPE_STEP.Form_Modal) {
    return <FormModal onClose={onClose} />;
  } else if (step === TYPE_STEP.DISSUASION) {
    return <CancellationConfirmation onClose={onClose} />;
  } else if (step === TYPE_STEP.ERROR) {
    return <Error onClose={onClose} />;
  } else if (step === TYPE_STEP.REGISTRATION_SUCCESS) {
    return <RegistrationSuccessModal onClose={onClose} />;
  } else if (step === TYPE_STEP.DISSUASION_MODAL) {
    return <DissuasionModal onClose={onClose} />;
  } else if (step === TYPE_STEP.SHOW_REQUEST) {
    return <ShowRequest onClose={onClose} />;
  } else {
    return null;
  }
}
