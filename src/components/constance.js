export const TYPE_STEP = {
  LOGIN: "login",
  OTP: "otp",
  SELECT_COIN: "selectCoin",
  REGISTRATION_SUCCESS: "registrationSuccess",
  Form_Modal: "formModal",
  DISSUASION: "dissuasion",
  DISSUASION_MODAL: "dissuasionModal",
  SHOW_REQUEST: "showRequest",
  ERROR: "error",
};
