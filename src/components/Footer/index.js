import {
  Box,
  Container,
  Grid,
  IconButton,
  styled,
  Typography,
  useTheme,
} from "@mui/material";
import {
  Aparat,
  Instagram,
  Linkedin,
  Telegram,
  Twitter,
} from "../../assets/Icon";
import ItemLink from "./Component/ItemLink";

function Footer() {
  const theme = useTheme();

  return (
    <BoxStyle>
      <Container>
        <Grid container direction="row-reverse">
          <Grid item md={3} xs={12} mt={3} textAlign="right">
            <Typography color="text.alternate" pb={{ md: 4, xs: 2 }}>
              فرم‌ها و دستورالعمل‌ها
            </Typography>

            <BoxContact>
              <ItemLink data=" قوانین و مقررات ارزی" link="/our-service" />

              <ItemLink data=" فرم های صرافی" link="/our-service" />

              <ItemLink data=" درخواست همکاری و استخدام" link="/our-service" />
            </BoxContact>
          </Grid>

          <Grid item md={3} xs={12} mt={3} textAlign="right">
            <Typography color="text.alternate" pb={{ md: 4, xs: 2 }}>
              فارابی
            </Typography>

            <BoxContact>
              <ItemLink data=" درباره ما" link="/about-us" />

              <ItemLink data=" خدمات صرافی فارابی" link="/our-service" />
            </BoxContact>
          </Grid>

          <Grid item md={4} xs={12} mt={3} textAlign="right">
            <Typography color="text.alternate" pb={{ md: 4, xs: 2 }}>
              تماس با ما
            </Typography>

            <BoxContact>
              <Typography color="text.main" lineHeight={2}>
                آدرس: شهرک قدس ،بلوار خورین پایین تر تقاطع دریا، نبش توحید سوم،
                پلاک3، طبقه اول(غرب)
              </Typography>

              <Typography color="text.main"> تلفن: ۰۲۱-۲۱۵۶۱۷۷۷</Typography>

              <Typography color="text.main">کد پستی:‌ ۱۴۶۶۹۹۷۳۹۱ </Typography>
            </BoxContact>
          </Grid>
        </Grid>

        <BoxBottom>
          <Grid container direction="row-reverse">
            <Grid
              item
              md={6}
              xs={12}
              textAlign={{ sm: "right", xs: "center" }}
              order={{ md: 1, xs: 2 }}
            >
              <Typography color="text.alternate" dir="rtl">
                © تمامی حقوق برای کارگزاری فارابی محفوظ است.
              </Typography>
            </Grid>

            <Grid
              item
              md={6}
              xs={12}
              textAlign={{ sm: "left", xs: "center" }}
              order={{ md: 2, xs: 1 }}
            >
              <BoxSocialNetworks dir="rtl">
                <IconButton href="https://twitter.com/farabifarabixo">
                  <Twitter color={theme.palette.text.alternate} />
                </IconButton>

                <IconButton href="">
                  <Aparat color={theme.palette.text.alternate} />
                </IconButton>

                <IconButton href="https://instagram.com/farabi_farabixo">
                  <Instagram color={theme.palette.text.alternate} />
                </IconButton>

                <IconButton href="https://t.me/farabiexchange">
                  <Telegram color={theme.palette.text.alternate} />
                </IconButton>

                <IconButton href="https://www.linkedin.com/company/farabi-brokerage-firm/">
                  <Linkedin color={theme.palette.text.alternate} />
                </IconButton>
              </BoxSocialNetworks>
            </Grid>
          </Grid>
        </BoxBottom>
      </Container>
    </BoxStyle>
  );
}

const BoxStyle = styled(Box)(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  padding: "51px 0px",
  gap: "72px",
  background: theme.palette.background.footer,
  [theme.breakpoints.down("sm")]: {
    padding: "43px 33px 32px",
  },
}));

const BoxContact = styled(Box)(({ theme }) => ({
  display: "flex",
  flexDirection: "column",
  gap: "20px",
  [theme.breakpoints.down("sm")]: {
    gap: "7px",
  },
}));

const BoxBottom = styled(Box)(({ theme }) => ({
  borderTop: "1px solid #919191",
  paddingTop: theme.spacing(5),
  marginTop: theme.spacing(5),
  [theme.breakpoints.down("sm")]: {
    paddingBottom: theme.spacing(8),
  },
}));

const BoxSocialNetworks = styled(Box)(({ theme }) => ({
  ".MuiButtonBase-root": {
    marginRight: theme.spacing(2),
  },
  [theme.breakpoints.down("lg")]: {
    paddingBottom: theme.spacing(3),
  },
  [theme.breakpoints.down("sm")]: {
    ".MuiButtonBase-root": {
      marginRight: theme.spacing(0),
    },
    display: "flex",
    justifyContent: "space-around",
  },
}));

export default Footer;
