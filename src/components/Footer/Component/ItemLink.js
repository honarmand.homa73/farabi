import { styled, Typography } from "@mui/material";
import { useNavigate } from "react-router-dom";

function ItemLink({ data, link }) {
  const navigate = useNavigate();

  return (
    <TypographyLink
      variant="text"
      onClick={() => navigate(link)}
      color="text.main"
    >
      {data}
    </TypographyLink>
  );
}

const TypographyLink = styled(Typography)(() => ({
  cursor: "pointer",
}));
export default ItemLink;
