import {
  Box,
  Container,
  Dialog,
  IconButton,
  Slide,
  Typography,
  styled,
  useTheme,
} from "@mui/material";

import { CloseSmall } from "../assets/Icon";
import { forwardRef } from "react";
import useIsMobileView from "../hooks/useIsMobileView";

const Transition = forwardRef(function Transition(props, ref) {
  const isMobileView = useIsMobileView();

  return (
    <Slide
      direction="up"
      ref={ref}
      {...props}
      sx={isMobileView ? { alignItems: "flex-end " } : { alignItems: "center" }}
    />
  );
});

export function Modal({
  onClose,
  open,
  title,
  hideContainer,
  maxWidth = "lg",
  children,
  dir = "ltr",
}) {
  const theme = useTheme();

  return (
    <DialogStyle
      open={open}
      TransitionComponent={Transition}
      keepMounted
      maxWidth={maxWidth}
      onClose={onClose}
      aria-describedby="alert-dialog-slide-description"
    >
      {hideContainer ? (
        <>{title}</>
      ) : (
        <DialogTitleWrapper dir={dir || "rtl"}>
          <Typography variant="h6" color="text.main">
            {title}
          </Typography>

          <IconButton onClick={onClose} color="text.main">
            <CloseSmall color={theme.palette.text.primary} size={30} />
          </IconButton>
        </DialogTitleWrapper>
      )}
      {hideContainer ? (
        <>{children}</>
      ) : (
        <DialogContainer>{children}</DialogContainer>
      )}
    </DialogStyle>
  );
}

const DialogStyle = styled(Dialog)(({ theme }) => ({
  ".MuiPaper-root": {
    width: "100%",
    borderRadius: "8px",
    margin: 0,
    [theme.breakpoints.down("md")]: {
      borderRadius: "8px 8px 0px 0",
    },
  },
}));

const DialogContainer = styled(Container)(({ theme }) => ({
  padding: theme.spacing(2),
}));

export const DialogTitleWrapper = styled(Box)(({ theme }) => ({
  padding: theme.spacing(1, 2),
  display: "flex",
  alignItems: "end",
  justifyContent: "space-between",
  background: "none",
  position: "absolute",
  top: "0",
  left: "-9px",
  zIndex: 1,
  "&:before": {
    content: "''",
    position: "absolute",
    top: "0",
    left: "0",
    width: "100%",
    background: theme.palette.common.white,
    height: "100%",
    zIndex: "-1",
  },
}));

export default Modal;
