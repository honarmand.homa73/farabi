import { Box, Button, styled, Typography } from "@mui/material";
import React from "react";
import { useNavigate } from "react-router-dom";
import useIsMobileView from "../../../hooks/useIsMobileView";
import ButtonCoin from "../../ButtonCoin";

export default function Navbar({ directionColumn, setOpenMenu }) {
  const navigate = useNavigate();
  const isMobileView = useIsMobileView();

  return (
    <BoxNavbar directionColumn={directionColumn}>
      <Button
        variant="text"
        onClick={() => {
          navigate("/our-service");
          setOpenMenu(false);
        }}
      >
        <Typography color="text.primary" fontWeight={{ sm: 700, md: 400 }}>
          خدمات ما
        </Typography>
      </Button>

      <Button
        variant="text"
        onClick={() => {
          navigate("/about-us");
          setOpenMenu(false);
        }}
      >
        <Typography color="text.primary" fontWeight={{ sm: 700, md: 400 }}>
          درباره ما
        </Typography>
      </Button>
      {!isMobileView && <ButtonCoin />}
    </BoxNavbar>
  );
}
const BoxNavbar = styled(Box)(({ theme, directionColumn }) => ({
  direction: "rtl",
  flexDirection: directionColumn && "column",
  display: directionColumn && "flex",
  alignItems: directionColumn && "baseline",
  padding: !directionColumn && "20px 0",
  ".MuiButton-root": {
    marginLeft: theme.spacing(3),
    fontSize: "12px",
    [theme.breakpoints.down("sm")]: {
      minWidth: "90px",
      display: "flex",
      justifyContent: "flex-start",
    },
  },
}));
