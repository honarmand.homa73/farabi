import {
  Box,
  Container,
  Grid,
  IconButton,
  styled,
  useTheme,
} from "@mui/material";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Menu } from "../../assets/Icon";
import Logo from "../../assets/logo/farabi.svg";
import useIsMobileView from "../../hooks/useIsMobileView";
import Modal from "../Modal";
import Navbar from "./Component/Navbar";

function Header() {
  const navigate = useNavigate();
  const isMobileView = useIsMobileView();
  const theme = useTheme();

  const [openMenu, setOpenMenu] = useState(false);

  const handleDrawerOpen = () => {
    setOpenMenu(true);
  };

  return (
    <>
      <Container maxWidth="lg">
        <Grid container direction="row-reverse">
          <Grid item sm={3} xs={6} textAlign="right">
            <LogoWrapper>
              <img
                className="main-logo"
                src={Logo}
                alt=""
                onClick={() => navigate("/")}
              />
            </LogoWrapper>
          </Grid>

          <Grid item sm={9} xs={6} textAlign={isMobileView ? "left" : "end"}>
            {isMobileView ? (
              <BoxIcon>
                <IconButton
                  color="inherit"
                  aria-label="open drawer"
                  onClick={handleDrawerOpen}
                  edge="start"
                >
                  <Menu color={theme.palette.secondary.main} />
                </IconButton>
              </BoxIcon>
            ) : (
              <Navbar />
            )}
          </Grid>
        </Grid>
      </Container>

      <Modal
        maxWidth="sm"
        dir="rtl"
        open={openMenu}
        onClose={() => setOpenMenu(false)}
      >
        <Grid item xs={12} textAlign="right">
          <img className="main-logo" src={Logo} alt="" />
        </Grid>
        <Grid item xs={12} textAlign="right">
          <Navbar directionColumn={true} setOpenMenu={setOpenMenu} />
        </Grid>
      </Modal>
    </>
  );
}

const LogoWrapper = styled(Box)(({ theme }) => ({
  textAlign: "center",

  ".main-logo": {
    [theme.breakpoints.down("sm")]: {
      padding: "11px 0",
    },
    display: "block",
    width: "119px",
    padding: "20px 0",
    objectFit: "contain",
    marginLeft: "auto",
    cursor: "pointer",
  },
}));

const BoxIcon = styled(Box)(({ theme }) => ({
  ".MuiIconButton-root": {
    paddingTop: theme.spacing(3),
    paddingLeft: theme.spacing(2),
    [theme.breakpoints.down("sm")]: {
      paddingTop: theme.spacing(1.3),
    },
  },
}));

export default Header;
