import jalaliMoment from "jalali-moment";
import { createPickerAdapter } from "@mui/x-date-pickers-pro";

export default createPickerAdapter({
  dateLibInstance: jalaliMoment,
  locale: "fa",
  DateTimeFormat: jalaliMoment,
  weekStartsOn: 6,
  dayOfMonthOrdinalParse: /\d{1,2}(ام|م)/,
  formats: {
    date: "jYYYY/jMM/jDD",
    dateTime: "jYYYY/jMM/jDD HH:mm:ss",
    time: "HH:mm:ss",
  },
});
