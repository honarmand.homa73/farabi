import { Alert, Box, styled, Typography, useTheme } from "@mui/material";
import { LoadingButton } from "@mui/lab";

import OtpInput from "../OtpInput";
import OtpPhone from "../../assets/images/OtpPhone.png";
import { Timer } from "../../assets/Icon";
import CountdownButton from "../CountdownButton";
import { useDispatch, useSelector } from "react-redux";
import { openModal } from "../../store/step";
import {
  CheckActivationCodeByMobileNumber,
  CreateActivationCodeByMobileNumber,
} from "../../store/api/login";
import http from "../../config/http";
import { useState } from "react";
import { setCodeOtp } from "../../store/login";

export default function OtpModal() {
  const theme = useTheme();
  const dispatch = useDispatch();

  const selectedCoin = useSelector((state) => state.selectedCoins.selectedCoin);
  const mobileNumber = useSelector((state) => state.login.mobileNumber);
  const codeOtp = useSelector((state) => state.login.codeOtp);
  const activeReserve = useSelector((state) => state.login.activeReserve);

  const [notificationError, setNotificationError] = useState(false);
  const [loading, setLoading] = useState(false);
  const [otp, setOtp] = useState("");

  const handleChange = (newValue) => {
    setOtp(newValue);
  };
  console.log("otp", codeOtp);

  const getCheckActivationCodeByMobileNumber = async () => {
    try {
      setLoading(true);
      await http.get(
        CheckActivationCodeByMobileNumber({ mobileNumber, codeOtp })
      );
      if (activeReserve) {
        dispatch(openModal("showRequest"));
      } else if (!selectedCoin) {
        dispatch(openModal("selectCoin"));
      } else {
        dispatch(openModal("formModal"));
      }
    } catch (e) {
      setNotificationError(e?.ErrorMessages[0]);
    }
    setLoading(false);
  };

  const handleGetCode = async () => {
    try {
      const { data } = await http.get(
        CreateActivationCodeByMobileNumber({ mobileNumber })
      );
      dispatch(setCodeOtp(data.Result));
      setOtp("");
    } catch (e) {
      setNotificationError(e?.ErrorMessages[0]);
    }
  };

  const handleClick = () => {
    if (otp === codeOtp) {
      getCheckActivationCodeByMobileNumber();
    } else {
      setNotificationError("کد اشتباه است");
    }
  };

  return (
    <>
      {notificationError && (
        <Alert
          severity="error"
          sx={{
            direction: "rtl",
            width: "100%",
            position: "relative",
            zIndex: 10,
            top: "32px",
            ".MuiAlert-icon": {
              marginLeft: "5px",
            },
          }}
        >
          {notificationError}
        </Alert>
      )}

      <BoxIcon>
        <Box>
          <img src={OtpPhone} alt="" />
        </Box>
      </BoxIcon>

      <Typography variant="h5" fontWeight={700} textAlign="center" mb={1}>
        کد دریافت شده را وارد کنید
      </Typography>

      <TypographyStyle
        variant="h5"
        textAlign="center"
        color="primary.main"
        onClick={() => dispatch(openModal("login"))}
      >
        {mobileNumber} ویرایش
      </TypographyStyle>

      <OtpInput handleChange={handleChange} otp={otp} />

      <Box
        display="flex"
        alignItems="center"
        justifyContent="center"
        mb={3}
        mt={1}
      >
        <Typography
          dir="rtl"
          variant="h6"
          textAlign="center"
          color="text.alternate"
          pr={1}
        >
          <CountdownButton onClick={handleGetCode} time={120} />
        </Typography>

        <Timer color={theme.palette.text.alternate} />
      </Box>

      <LoadingButton
        variant="contained"
        fullWidth
        loading={loading}
        color="success"
        onClick={handleClick}
      >
        تایید و ادامه
      </LoadingButton>
    </>
  );
}

const BoxIcon = styled(Box)(({ theme }) => ({
  display: "flex",
  justifyContent: "center",
  paddingTop: "40px",
  paddingBottom: "24px",
  ".MuiBox-root ": {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "flex-start",
    padding: "15px",
    gap: "10px",
    width: "88px",
    height: "88px",
    background: "#F8F8F8",
    borderRadius: "50%",
  },
}));

const TypographyStyle = styled(Typography)(({ theme }) => ({
  borderBottom: `1px solid ${theme.palette.success.main}20`,
  cursor: "pointer",
  maxWidth: "172px",
  margin: "auto",
  marginBottom: theme.spacing(4),
  marginTop: theme.spacing(2),
}));
