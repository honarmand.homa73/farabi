import { Box, Button, Grid, styled, Typography, useTheme } from "@mui/material";
import { Danger } from "../../assets/Icon";
import { useDispatch, useSelector } from "react-redux";
import { PersianNum } from "../../utils/helpers/PersianNum";
import { selectCurrencyCategory } from "./constance";
import { useMemo } from "react";
import { openModal } from "../../store/step";
import { setMobileNumber } from "../../store/login";

export default function Error() {
  const theme = useTheme();
  const dispatch = useDispatch();
  const informationUser = useSelector(
    (state) => state.informationUser.information
  );

  const selectDateReservation = useMemo(() => {
    if (informationUser?.From !== undefined) {
      return new Date(informationUser?.From)?.toLocaleDateString("fa-IR");
    } else {
      return "";
    }
  }, [informationUser]);

  const formattedTime = useMemo(() => {
    if (informationUser?.From !== undefined) {
      return `${new Date(informationUser?.From)
        ?.toISOString()
        ?.split("T")[1]
        ?.split(".")[0]
        .split(":", 2)
        .join(":")} تا ${new Date(informationUser?.To)
        ?.toISOString()
        ?.split("T")[1]
        ?.split(".")[0]
        .split(":", 2)
        .join(":")}`;
    } else {
      return "";
    }
  }, [informationUser]);

  const backForm = () => {
    dispatch(openModal("formModal"));
  };

  return (
    <>
      <BoxIcon>
        <Box>
          <Danger color={theme.palette.error.main} size={60} />
        </Box>
      </BoxIcon>

      <Typography
        variant="h5"
        fontWeight={700}
        textAlign="center"
        mb={5}
        dir="rtl"
      >
        متاسفانه ثبت درخواست زیر با خطا مواجه شد!
      </Typography>

      <BoxWrap>
        <Box>
          <Typography
            variant="h6"
            textAlign="center"
            mb={1}
            color="text.secondary"
          >
            نام
          </Typography>
          <Typography variant="h5" fontWeight={700} textAlign="center" mb={1}>
            {informationUser?.FirstName}
          </Typography>
        </Box>

        <Box>
          <Typography
            variant="h6"
            textAlign="center"
            mb={1}
            color="text.secondary"
          >
            نام خانوادگی
          </Typography>
          <Typography variant="h5" fontWeight={700} textAlign="center" mb={1}>
            {informationUser?.LastName}
          </Typography>
        </Box>
      </BoxWrap>

      <BoxWrap>
        <Box>
          <Typography
            variant="h6"
            textAlign="center"
            mb={1}
            color="text.secondary"
          >
            کد ملی
          </Typography>
          <Typography variant="h5" fontWeight={700} textAlign="center" mb={1}>
            {PersianNum(informationUser?.NationalCode)}
          </Typography>
        </Box>

        <Box>
          <Typography
            variant="h6"
            textAlign="center"
            mb={1}
            color="text.secondary"
          >
            بابت
          </Typography>
          <Typography variant="h5" fontWeight={700} textAlign="center" mb={1}>
            {selectCurrencyCategory(informationUser?.CurrencyCategory)}
          </Typography>
        </Box>
      </BoxWrap>

      <BoxWrap display="flex">
        <Grid container spacing={2} pb={2}>
          <Grid item xs={12} textAlign="right">
            <Typography variant="h6" mb={1}>
              شماره حساب ارزی{" "}
            </Typography>

            <Typography variant="h5" fontWeight={700}>
              {PersianNum(informationUser?.ForeignCurrency)}
            </Typography>
          </Grid>
        </Grid>
      </BoxWrap>

      <BoxWrap>
        <Box>
          <Typography
            variant="h6"
            textAlign="center"
            mb={1}
            color="text.secondary"
          >
            تاریخ مراجعه
          </Typography>
          <Typography variant="h5" fontWeight={700} textAlign="center" mb={1}>
            {PersianNum(selectDateReservation)}
          </Typography>
        </Box>

        <Box>
          <Typography
            variant="h6"
            textAlign="center"
            mb={1}
            color="text.secondary"
          >
            ساعت مراجعه
          </Typography>
          <Typography variant="h5" fontWeight={700} textAlign="center" mb={1}>
            {PersianNum(formattedTime)}
          </Typography>
        </Box>
      </BoxWrap>

      <Box display="flex" dir="rtl">
        <Button
          fullWidth
          color="text"
          onClick={() => {
            dispatch(openModal(""));
            dispatch(setMobileNumber(""));
          }}
        >
          {" "}
          انصراف و بستن
        </Button>
        <Button fullWidth color="success" onClick={backForm}>
          تلاش مجدد{" "}
        </Button>
      </Box>
    </>
  );
}

const BoxIcon = styled(Box)(({ theme }) => ({
  display: "flex",
  justifyContent: "center",
  paddingTop: "20px",
  paddingBottom: "24px",
  ".MuiBox-root ": {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "flex-start",
    padding: "15px",
    gap: "10px",
    width: "88px",
    height: "88px",
    background: "#FDEFED",
    borderRadius: "50%",
  },
}));

const BoxWrap = styled(Box)(({ theme }) => ({
  display: "flex",
  direction: "rtl",
  borderBottom: "1px solid #F0F0F0",
  marginBottom: theme.spacing(2),

  ".MuiBox-root": {
    flexBasis: "50%",
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    paddingBottom: theme.spacing(2),
  },
}));
