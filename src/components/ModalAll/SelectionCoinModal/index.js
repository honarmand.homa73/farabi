import { Alert, Typography, styled } from "@mui/material";
import { useEffect, useState } from "react";
import BoxCoinSelect from "./Component/BoxCoinSelect";
import { GetCurrencyLists } from "../../../store/api/createReservation";
import http from "../../../config/http";
import { setSelectedCoin } from "store/selectCoin";
import { useDispatch } from "react-redux";

export default function SelectionCoinModal() {
  const [currencyLists, setCurrencyLists] = useState([]);
  const [textError, setTextError] = useState("");
  const dispatch = useDispatch();

  const getCurrencyLists = async () => {
    try {
      const { data } = await http.get(GetCurrencyLists);
      setCurrencyLists(data?.Result);
      dispatch(setSelectedCoin(data?.Result));
    } catch (e) {
      setTextError(e?.ErrorMessages[0]);
    }
  };

  useEffect(() => {
    getCurrencyLists();
  }, []);
  const sortedCurrencyLists = currencyLists
    .slice()
    .sort((a, b) => a.Order - b.Order);
  return (
    <>
      {textError && (
        <AlertStyle
          severity="error"
          sx={{
            direction: "rtl",
            width: "100%",
            position: "relative",
            zIndex: 10,
            top: "32px",
            ".MuiAlert-icon": {
              marginLeft: "5px",
            },
          }}
        >
          {textError}
        </AlertStyle>
      )}
      <Typography
        variant="h5"
        fontWeight={700}
        textAlign="center"
        mb={4}
        mt={6}
      >
        ارز مورد نظر خود را انتخاب کنید
      </Typography>
      {sortedCurrencyLists?.map((coin) => (
        <BoxCoinSelect
          coin={coin}
          status={+coin?.Enable}
          type={coin?.Order}
          icon={coin?.Image}
          title={coin?.Title}
        />
      ))}
    </>
  );
}

const AlertStyle = styled(Alert)(({ theme }) => ({
  [theme.breakpoints.down("md")]: {
    position: "fixed",
    top: 0,
    left: 0,
    right: 0,
  },
}));
