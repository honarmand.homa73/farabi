import { Box, styled, Typography, useTheme } from "@mui/material";
import { Danger, Left } from "../../../../assets/Icon";
import { STATUS_COIN, TYPE_COIN } from "../../../../pages/Home/constants";
import { useDispatch } from "react-redux";
import { setSelectedCoin } from "../../../../store/selectCoin";
import { openModal } from "../../../../store/step";
import USD from "../../../../assets/images/coin/USD.png";
import EUR from "../../../../assets/images/coin/EUR.png";
import gold from "../../../../assets/images/coin/gold.png";
import { useMemo } from "react";

export default function BoxCoinSelect({ status, type, icon, title, coin }) {
  const theme = useTheme();
  const dispatch = useDispatch();

  const showCoin = useMemo(() => {
    if (coin?.Title === TYPE_COIN.USD) {
      return USD;
    } else if (coin?.Title === TYPE_COIN.EUR) {
      return EUR;
    } else if (coin?.Title === TYPE_COIN.COIN) {
      return gold;
    } else {
      return "DER";
    }
  }, [coin]);

  const handleSetCoin = () => {
    if (status !== STATUS_COIN.inactive) {
      if (type === coin?.Order) {
        dispatch(setSelectedCoin(coin));
        dispatch(openModal("formModal"));
      } else {
        return "";
      }
    }
  };

  return (
    <BoxStyle status={status} onClick={handleSetCoin}>
      <Box display="flex">
        <img
          src={icon !== undefined ? { icon } : showCoin}
          alt=""
          width="34px"
          height="24px"
        />
        <Typography
          variant="h5"
          fontWeight={600}
          pr={1}
          color={
            status === STATUS_COIN.available ? "text.primary" : "text.secondary"
          }
        >
          {title}
        </Typography>
        <Typography
          variant="h5"
          color={
            status === STATUS_COIN.inactive
              ? "text.alternate"
              : "text.secondary"
          }
          px={0.5}
        ></Typography>
      </Box>
      {status === STATUS_COIN.available && (
        <Left color={theme.palette.text.secondary} />
      )}
      {status === STATUS_COIN.inactive && (
        <Typography color={theme.palette.text.secondary}>
          موقتا غیرفعال!
        </Typography>
      )}
      {status === STATUS_COIN.fullCapacity && (
        <Box display="flex" alignItems="center">
          <Typography color="primary.secondary" pl={0.5}>
            تکمیل ظرفیت
          </Typography>
          <Danger color={theme.palette.primary.secondary} />
        </Box>
      )}
    </BoxStyle>
  );
}

const BoxStyle = styled(Box)(({ theme, status }) => ({
  display: "flex",
  flexDirection: "row",
  justifyContent: "space-between",
  alignItems: "center",
  padding: "16px 8px",
  background: status === STATUS_COIN.inactive ? "#F0F0F0" : "#FFFFFF",
  border: `1px solid ${theme.palette.modules.border}`,
  borderRadius: "8px",
  direction: "rtl",
  marginBottom: theme.spacing(2),
  cursor: "pointer",
}));
