import { Box, Button, styled, Typography, useTheme } from "@mui/material";
import { Success } from "../../assets/Icon";
import { useDispatch, useSelector } from "react-redux";
import { openModal } from "../../store/step";
import { selectCurrencyCategory } from "./constance";
import { useMemo } from "react";
import { PersianNum } from "../../utils/helpers/PersianNum";
import { setMobileNumber } from "../../store/login";

export default function RegistrationSuccessModal() {
  const theme = useTheme();
  const dispatch = useDispatch();
  const trackingCode = useSelector((state) => state.login.trackingCode);
  const userInformation = useSelector((state) => state.informationUser.user);
  const recordTimeReservation = useSelector(
    (state) => state.informationUser.recordTimeReservation
  );

  const selectDateReservation = useMemo(() => {
    if (recordTimeReservation !== undefined) {
      return new Date(recordTimeReservation?.From)?.toLocaleDateString("fa-IR");
    } else {
      return "";
    }
  }, [recordTimeReservation]);

  const formattedTime = useMemo(() => {
    if (recordTimeReservation !== undefined) {
      return `${new Date(recordTimeReservation?.From)
        ?.toISOString()
        ?.split("T")[1]
        ?.split(".")[0]
        .split(":", 2)
        .join(":")} تا ${new Date(recordTimeReservation?.To)
        ?.toISOString()
        ?.split("T")[1]
        ?.split(".")[0]
        .split(":", 2)
        .join(":")}`;
    } else {
      return "";
    }
  }, [recordTimeReservation]);

  return (
    <>
      <BoxIcon>
        <Box>
          <Success color={theme.palette.primary.main} size={60} />
        </Box>
      </BoxIcon>

      <Typography
        variant="h5"
        fontWeight={700}
        textAlign="center"
        mb={4}
        dir="rtl"
      >
        درخواست شما با موفقیت ثبت شد.
      </Typography>

      <Typography
        display="flex"
        color="success.main"
        variant="h5"
        fontWeight={700}
        justifyContent="center"
        mb={4}
        dir="rtl"
      >
        شماره پیگیری:
        <Typography variant="h5" fontWeight={700} pr={1}>
          {PersianNum(trackingCode)}
        </Typography>
      </Typography>

      <BoxWrap>
        <Box>
          <Typography variant="h6" textAlign="center" mb={1}>
            نام
          </Typography>
          <Typography variant="h5" fontWeight={700} textAlign="center" mb={1}>
            {userInformation?.firstName}
          </Typography>
        </Box>

        <Box>
          <Typography variant="h6" textAlign="center" mb={1}>
            نام خانوادگی
          </Typography>
          <Typography variant="h5" fontWeight={700} textAlign="center" mb={1}>
            {userInformation?.lastName}
          </Typography>
        </Box>
      </BoxWrap>

      <BoxWrap>
        <Box>
          <Typography variant="h6" textAlign="center" mb={1}>
            کد ملی
          </Typography>
          <Typography variant="h5" fontWeight={700} textAlign="center" mb={1}>
            {PersianNum(userInformation?.nationalCode)}
          </Typography>
        </Box>

        <Box>
          <Typography variant="h6" textAlign="center" mb={1}>
            بابت
          </Typography>
          <Typography variant="h5" fontWeight={700} textAlign="center" mb={1}>
            {selectCurrencyCategory(userInformation?.CurrencyCategory)}
          </Typography>
        </Box>
      </BoxWrap>

      <BoxWrap>
        <Box>
          <Typography variant="h6" textAlign="center" mb={1}>
            تاریخ مراجعه
          </Typography>
          <Typography variant="h5" fontWeight={700} textAlign="center" mb={1}>
            {selectDateReservation}
          </Typography>
        </Box>

        <Box>
          <Typography variant="h6" textAlign="center" mb={1}>
            ساعت مراجعه
          </Typography>
          <Typography variant="h5" fontWeight={700} textAlign="center" mb={1}>
            {PersianNum(formattedTime)}
          </Typography>
        </Box>
      </BoxWrap>

      <BoxAlert>
        <Typography
          variant="h6"
          textAlign="center"
          mb={1}
          color="primary.secondary"
        >
          * داشتن کارت ملی به هنگام مراجعه حضوری الزامی است.
        </Typography>
      </BoxAlert>
      <Button
        fullWidth
        color="success"
        onClick={() => dispatch(openModal(""), dispatch(setMobileNumber("")))}
      >
        متوجه شدم
      </Button>
    </>
  );
}

const BoxIcon = styled(Box)(({ theme }) => ({
  display: "flex",
  justifyContent: "center",
  paddingTop: "20px",
  paddingBottom: "24px",
  ".MuiBox-root ": {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "flex-start",
    padding: "15px",
    gap: "10px",
    width: "88px",
    height: "88px",
    background: "#EEFBF5",
    borderRadius: "50%",
  },
}));

const BoxWrap = styled(Box)(({ theme }) => ({
  display: "flex",
  direction: "rtl",
  borderBottom: "1px solid #F0F0F0",
  marginBottom: theme.spacing(2),

  ".MuiBox-root": {
    flexBasis: "50%",
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    paddingBottom: theme.spacing(2),
  },
}));

const BoxAlert = styled(Box)(({ theme }) => ({
  display: "flex",
  direction: "rtl",
  background: "#F6F1E2",
  marginBottom: theme.spacing(2),
  padding: theme.spacing(1),
}));
