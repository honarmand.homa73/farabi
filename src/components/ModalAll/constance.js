export const CurrencyCategory = [
  { value: 3, name: "امور دانشجویی (داشتن کد ناخدا اجباریست) " },
  { value: 4, name: "مسافرت (پاسپورت، بلیط، عوارض خروج)  " },
  { value: 5, name: "ماموریت شرکتی" },
  { value: 6, name: "امور درمانی" },
  { value: 7, name: "حق عضویت ، حق ثبت نام و هزینه چاپ مقاله" },
  { value: 8, name: "خریدهای خرد کالایی" },
  { value: 9, name: "پرداخت دیون" },
  { value: 10, name: "فرصت مطالعاتی" },
  { value: 11, name: "هزینه شرکت در نمایشگاه های خارج از کشور" },
  { value: 12, name: "حق الوکاله در رابطه با دعاوی خارجی" },
  { value: 13, name: "هزینه دفاتر خارج از کشور" },
  { value: 14, name: "هزینه اجاره و اشتراک شبکه های اطلاعاتی" },
  { value: 15, name: "هزینه انتشار آگهی در خارج از کشور" },
  { value: 16, name: "هزینه ثبت نام جهت شرکت در آزمون های بین المللی" },
  { value: 17, name: "حقوق و مزایای کارکنان خارجی" },
  { value: 18, name: "رانندگان بخش حمل و نقل بین المللی" },
  { value: 19, name: "هزینه شرکت در دوره های آموزشی و پژوهشی خارج از کشور" },
  {
    value: 20,
    name: "هزینه ثبت نام شرکت در امتحانات علمی و تخصصی داخل و خارج از کشور",
  },
  { value: 21, name: "هزینه های حمل و ترانزیت" },
  { value: 22, name: "سایر" },
];

export const selectCurrencyCategory = (idCurrencyCategory) => {
  const nameCurrencyCategory = CurrencyCategory.find((item) => {
    if (idCurrencyCategory === item.value) {
      return item.name;
    }
  });
  return nameCurrencyCategory?.name;
};
