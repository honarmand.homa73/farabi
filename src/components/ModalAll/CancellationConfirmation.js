import { Box, Button, Typography, styled } from "@mui/material";
import EmptyState from "../../assets/images/EmptyState.png";
import { useSelector, useDispatch } from "react-redux";

import { openModal } from "../../store/step";
import { useState } from "react";

export default function CancellationConfirmation() {
  const activeReserve = useSelector((state) => state.login.activeReserve);
  const [index, setIndex] = useState(0);
  const dispatch = useDispatch();

  const cancelReservation = () => {
    dispatch(openModal(""));
    setIndex(index + 1);
  };

  return (
    <>
      <BoxIcon>
        <img src={EmptyState} alt="" />
      </BoxIcon>

      <Typography
        variant="h5"
        fontWeight={800}
        textAlign="center"
        mb={3}
        lineHeight={2}
        dir="rtl"
      >
        کاربر گرامی <br />
        {activeReserve
          ? `در خواست شما با موفقیت لغو شد!
           در حال حاظر هیچ درخواستی در سامانه ندارید`
          : "با انصراف از ثبت درخواست، شما هیچ درخواست ثبت شده‌ای در سامانه صرافی فارابی ندارید!"}
      </Typography>

      <Button fullWidth color="success" onClick={cancelReservation}>
        متوجه شدم
      </Button>
    </>
  );
}

const BoxIcon = styled(Box)(({ theme }) => ({
  display: "flex",
  justifyContent: "center",
  paddingTop: "40px",
  paddingBottom: "24px",
  ".MuiBox-root ": {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "flex-start",
    padding: "15px",
    gap: "10px",

    borderRadius: "50%",
  },
}));
