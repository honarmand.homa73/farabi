import { LoadingButton } from "@mui/lab";
import {
  Alert,
  Box,
  Button,
  Grid,
  IconButton,
  TextField,
  Typography,
  styled,
} from "@mui/material";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useDispatch, useSelector } from "react-redux";
import { useTheme } from "@emotion/react";
import { useMemo, useState, useEffect } from "react";

import DatePicker from "../../DatePicker";
import Select from "../../Select";
import { AddEditRegistration } from "../../../utils/validations/AddEditRegistration";
import http from "../../../config/http";
import {
  CancelReservation,
  GetDaysStatus,
  GetIntervalStatusByDay,
  GetLoginActivityList,
} from "../../../store/api/createReservation";
import { openModal } from "../../../store/step";
import { setTrackingCode } from "../../../store/login";
import TitleForm from "./Component/TitleForm";
import { Trash } from "../../../assets/Icon";
import {
  setInformationUser,
  setRecordTimeReservation,
  setUser,
} from "../../../store/informationUser";
import { CurrencyCategory, selectCurrencyCategory } from "../constance";
import { enabledDates } from "./Component/enabledDates";
// import { PersianNum } from "../../../utils/helpers/PersianNum";

export default function FormModal() {
  const mobileNumber = useSelector((state) => state.login.mobileNumber);
  const selectedCoin = useSelector((state) => state.selectedCoins.selectedCoin);
  const activeReserve = useSelector((state) => state.login.activeReserve);
  const informationUser = useSelector(
    (state) => state.informationUser.information
  );
  const idReservation = informationUser?.ID;

  const theme = useTheme();
  const dispatch = useDispatch();
  const duration = 10;

  const [time, setTime] = useState([]);
  const [selectTime, setSelectTime] = useState();
  const [dates, setDates] = useState([]);
  const [selectDate, setSelectDate] = useState(new Date());
  const [textError, setTextError] = useState("");
  const [loading, setLoading] = useState(false);

  const handleChangeCurrency = (e) => {
    const value = e?.target?.value;
    setValue("CurrencyCategory", value);
  };

  const handleChangeTime = (e) => {
    const value = e?.target?.value;
    setValue("dayTurnID", value);
    setSelectTime(value);
    setTextError();
  };

  const getDaysStatus = async () => {
    try {
      const { data } = await http.get(GetDaysStatus({ duration }));
      if (data.Result !== null) {
        setDates(data.Result);
      } else {
        setTextError("در بازه زمانی 10 روز آینده روز کاری فعالی وجود ندارد");
      }
    } catch (e) {
      setTextError(e?.ErrorMessages[0]);
    }
  };

  const {
    handleSubmit,
    watch,
    trigger,
    setValue,
    register,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(AddEditRegistration()),
    defaultValues: {
      firstName: informationUser?.FirstName || "",
      lastName: informationUser?.LastName || "",
      nationalCode: informationUser?.NationalCode || "",
      dayTurnID: "",
      CurrencyCategory: informationUser?.CurrencyCategory || 3,
      // foreignCurrency: informationUser?.ForeignCurrency || "",
      mobilePhone: informationUser?.MobilePhone || "",
      type: informationUser?.Type || "",
      Currency: informationUser?.Currency || "",
    },
  });

  const onSubmit = async (data) => {
    const body = {
      firstName: data.firstName || informationUser?.FirstName,
      lastName: data.lastName || informationUser?.LastName,
      nationalCode: data.nationalCode || informationUser?.NationalCode,
      mobilePhone: mobileNumber || informationUser?.MobilePhone,
      dayTurnID: selectTime,
      type: "173400003",
      Currency: selectedCoin || informationUser?.Currency,
      CurrencyCategory:
        data.CurrencyCategory || informationUser?.CurrencyCategory,
      foreignCurrency: data.foreignCurrency || informationUser?.ForeignCurrency,
    };
    dispatch(
      setUser({
        ...body,
        CurrencyCategoryName: selectCurrencyCategory(data?.CurrencyCategory),
      })
    );

    try {
      setLoading(true);
      const { data } = await http.post(GetLoginActivityList, body);

      if (data?.Result === null) {
        setTextError(data?.ErrorMessages[0]);
      } else {
        await http.get(CancelReservation({ idReservation }));
        dispatch(openModal("registrationSuccess"));
        dispatch(setTrackingCode(data?.Result?.ID));
        dispatch(setInformationUser({}));
      }
    } catch (e) {
      dispatch(openModal("error"));
    }
    setLoading(false);
  };

  const times = useMemo(() => {
    const timeReservation = dates?.filter((item) => {
      if (
        new Date(item?.ReserveDate).toISOString().split("T")[0] ===
          new Date(selectDate).toISOString().split("T")[0] &&
        item?.Flag
      )
        return item?.ReserveDate;
      else return "";
    });
    return timeReservation;
  }, [selectDate]);

  const selectDateReservation = useMemo(() => {
    if (times[0]?.ReserveDate === undefined) {
      return new Date(selectDate)?.toISOString().split("T")[0];
    } else {
      return new Date(times[0]?.ReserveDate)?.toISOString()?.split("T")[0];
    }
  }, [selectDate]);

  const getIntervalStatusByDay = async () => {
    setTime([]);
    try {
      const { data } = await http.get(
        GetIntervalStatusByDay({ selectDateReservation })
      );
      if (data.Result !== null) {
        setTextError("");
        setTime(data?.Result);
      } else {
        setTextError(data?.ErrorMessages[0]);
      }
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getDaysStatus();
    getIntervalStatusByDay();
  }, [selectDate]);

  const formattedTime = useMemo(() => {
    const dateAll = time?.map((option) => {
      return {
        name: `${new Date(option?.From)
          ?.toISOString()
          ?.split("T")[1]
          ?.split(".")[0]
          .split(":", 2)
          .join(":")} تا ${new Date(option?.To)
          ?.toISOString()
          ?.split("T")[1]
          ?.split(".")[0]
          .split(":", 2)
          .join(":")}`,
        id: option?.ID,
      };
    });
    return dateAll;
  }, [time]);

  const timeReservation = useMemo(
    () => time?.find((item) => item?.ID === selectTime),
    [time, selectTime]
  );
  dispatch(setRecordTimeReservation(timeReservation));

  return (
    <>
      <TitleForm selectedCoin={selectedCoin} activeReserve={activeReserve} />
      {textError && (
        <AlertStyle
          severity="error"
          sx={{
            direction: "rtl",
            width: "100%",
            position: "relative",
            zIndex: 10,
            top: "32px",
            ".MuiAlert-icon": {
              marginLeft: "5px",
            },
          }}
        >
          {textError}
        </AlertStyle>
      )}
      <form onSubmit={handleSubmit(onSubmit)}>
        <Box dir="rtl">
          <Grid container spacing={2} py={5}>
            <Grid item md={6} xs={12} textAlign="right">
              <Typography variant="label">نام</Typography>

              <TextField
                rowsMin={1}
                placeholder="نام خود را وارد کنید "
                error={!!errors?.firstName}
                helperText={errors?.firstName?.message}
                value={watch("firstName")}
                disabled={activeReserve && true}
                onChange={async (e) => {
                  setValue("firstName", e.target.value);
                  await trigger("firstName");
                }}
              />
            </Grid>

            <Grid item md={6} xs={12} textAlign="right">
              <Typography variant="label">نام خانوادگی </Typography>

              <TextField
                placeholder="نام خانوادگی خود را وارد کنید"
                rowsMin={1}
                error={!!errors?.lastName}
                helperText={errors?.lastName?.message}
                value={watch("lastName")}
                onChange={async (e) => {
                  setValue("lastName", e.target.value);
                  await trigger("lastName");
                }}
                disabled={activeReserve && true}
              />
            </Grid>

            <Grid item md={6} xs={12} textAlign="right">
              <Typography variant="label">کد ملی</Typography>

              <TextField
                placeholder="کد ملی  خود را وارد کنید"
                rowsMin={1}
                error={!!errors?.nationalCode}
                helperText={errors?.nationalCode?.message}
                value={watch("nationalCode")}
                onChange={async (e) => {
                  setValue("nationalCode", e.target.value);
                  await trigger("nationalCode");
                }}
                disabled={activeReserve && true}
              />
            </Grid>

            <Grid item md={6} xs={12} textAlign="right">
              <Typography variant="label"> بابت</Typography>

              <Select
                disabled={activeReserve && true}
                itemText="name"
                itemValue="value"
                dir="rtl"
                items={CurrencyCategory}
                value={watch("CurrencyCategory")}
                onChange={handleChangeCurrency}
              />
            </Grid>

            <Grid item xs={12} textAlign="right">
              <Typography variant="label">شماره حساب ارزی </Typography>

              <TextField
                placeholder="شماره حساب ارزی خود را وارد کنید"
                rowsMin={1}
                disabled={activeReserve && true}
                error={!!errors?.foreignCurrency}
                helperText={errors?.foreignCurrency?.message}
                value={watch("foreignCurrency")}
                onChange={async (e) => {
                  setValue("foreignCurrency", e.target.value);
                  await trigger("foreignCurrency");
                }}
              />
            </Grid>

            <Grid item xs={12} md={6}>
              <Typography variant="label" color="text.secondary">
                انتخاب تاریخ مراجعه
              </Typography>

              <DatePicker
                value={selectDate}
                onChange={(e) => {
                  setSelectDate(e);
                }}
                enabledDates={enabledDates}
              />
            </Grid>

            <Grid item md={6} xs={12} textAlign="right">
              <Typography variant="label"> انتخاب ساعت مراجعه</Typography>

              <Select
                itemText="name"
                itemValue="id"
                dir="rtl"
                items={formattedTime}
                value={watch("dayTurnID")}
                {...register("dayTurnID")}
                error={!!errors?.dayTurnID}
                helperText={errors?.dayTurnID?.message}
                onChange={handleChangeTime}
              />
            </Grid>

            {activeReserve && (
              <Grid item xs={12}>
                <IconButton
                  onClick={() => dispatch(openModal("dissuasionModal"))}
                >
                  <Trash color={theme.palette.error.main} />
                  <Typography color="error.main">لغو درخواست</Typography>
                </IconButton>
              </Grid>
            )}

            <Grid item xs={6}>
              <ButtonStyle
                fullWidth
                onClick={() => dispatch(openModal("dissuasion"))}
              >
                انصراف و بستن
              </ButtonStyle>
            </Grid>

            <Grid item xs={6}>
              <LoadingButton
                variant="contained"
                fullWidth
                loading={loading}
                color="success"
                type="submit"
              >
                {activeReserve ? "اعمال تغییرات" : " ثبت درخواست"}
              </LoadingButton>
            </Grid>
          </Grid>
        </Box>
      </form>
    </>
  );
}
const ButtonStyle = styled(Button)(({ theme }) => ({
  background: theme.palette.common.white,
  color: theme.palette.success.main,
}));

const AlertStyle = styled(Alert)(({ theme }) => ({
  [theme.breakpoints.down("md")]: {
    position: "fixed",
    top: 0,
    left: 0,
    right: 0,
  },
}));
