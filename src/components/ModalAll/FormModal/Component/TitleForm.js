import USD from "../../../../assets/images/coin/USD.png";
import EUR from "../../../../assets/images/coin/EUR.png";
import gold from "../../../../assets/images/coin/gold.png";
import { TYPE_COIN } from "../../../../pages/Home/constants";
import { useMemo } from "react";
import { Box, Typography } from "@mui/material";
import { useSelector } from "react-redux";

export default function TitleForm({ activeReserve, ShowRequest }) {
  const selectedCoin = useSelector((state) => state.selectedCoins.selectedCoin);

  const showCoin = useMemo(() => {
    if (selectedCoin?.Title === TYPE_COIN.USD) {
      return USD;
    } else if (selectedCoin?.Title === TYPE_COIN.EUR) {
      return EUR;
    } else if (selectedCoin?.Title === TYPE_COIN.COIN) {
      return gold;
    } else {
      return "DER";
    }
  }, [selectedCoin]);

  const Coin = useMemo(() => {
    if (selectedCoin?.Title === TYPE_COIN.USD) {
      return "USD";
    } else if (selectedCoin?.Title === TYPE_COIN.EUR) {
      return "EUR";
    } else if (selectedCoin?.Title === TYPE_COIN.COIN) {
      return "GOLD";
    } else {
      return "DER";
    }
  }, [selectedCoin]);

  return (
    <Box display="flex" justify-content="flex-start" dir="rtl" pt={2}>
      <img src={showCoin} alt="" width="34px" height="24px" />
      <Typography variant="h5" fontWeight={600} pr={1} color="text.secondary">
        {ShowRequest
          ? "نوبت فعلی شما برای خرید"
          : `${activeReserve ? "ویرایش" : "ثبت"} "درخواست خرید" `}{" "}
        {selectedCoin?.Title}
      </Typography>
      {/* <Typography variant="h5" color="text.secondary" px={0.5}>
        / {Coin}
      </Typography> */}
    </Box>
  );
}
