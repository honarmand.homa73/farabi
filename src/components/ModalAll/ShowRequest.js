import { Box, Button, Grid, Typography, styled } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import TitleForm from "./FormModal/Component/TitleForm";
import { closeModal, openModal } from "../../store/step";
import { LoadingButton } from "@mui/lab";
import { useEffect, useMemo } from "react";
import { GetActiveReserveByMobile } from "../../store/api/login";
import http from "../../config/http";
import { setInformationUser } from "../../store/informationUser";
import { selectCurrencyCategory } from "./constance";
import { PersianNum } from "../../utils/helpers/PersianNum";
import { setMobileNumber } from "../../store/login";

export default function ShowRequest() {
  const mobileNumber = useSelector((state) => state.login.mobileNumber);
  const informationUser = useSelector(
    (state) => state.informationUser.information
  );
  const activeReserve = useSelector((state) => state.login.activeReserve);

  const dispatch = useDispatch();

  const handleOnClose = () => {
    dispatch(closeModal());
    dispatch(setMobileNumber(""));
  };

  const getActiveReserveByMobile = async () => {
    try {
      const { data } = await http.get(
        GetActiveReserveByMobile({ mobileNumber })
      );
      dispatch(setInformationUser(data?.Result));
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getActiveReserveByMobile();
  }, []);

  const selectDateReservation = useMemo(() => {
    if (informationUser?.From !== undefined) {
      return new Date(informationUser?.From)?.toLocaleDateString("fa-IR");
    } else {
      return "";
    }
  }, [informationUser]);

  const formattedTime = useMemo(() => {
    if (informationUser?.From !== undefined) {
      return `${new Date(informationUser?.From)
        ?.toISOString()
        ?.split("T")[1]
        ?.split(".")[0]
        .split(":", 2)
        .join(":")} تا ${new Date(informationUser?.To)
        ?.toISOString()
        ?.split("T")[1]
        ?.split(".")[0]
        .split(":", 2)
        .join(":")}`;
    } else {
      return "";
    }
  }, [informationUser]);

  return (
    <>
      <TitleForm activeReserve={activeReserve} ShowRequest={true} />

      <Box dir="rtl" pt={5}>
        <BoxWrap display="flex">
          <Grid container spacing={2} pb={2}>
            <Grid item md={6} xs={12} textAlign="right">
              <Typography variant="h6" mb={1}>
                نام
              </Typography>

              <Typography variant="h5" fontWeight={700}>
                {informationUser?.FirstName}
              </Typography>
            </Grid>

            <Grid item md={6} xs={12} textAlign="right">
              <Typography variant="h6" mb={1}>
                نام خانوادگی{" "}
              </Typography>

              <Typography variant="h5" fontWeight={700}>
                {informationUser?.LastName}
              </Typography>
            </Grid>
          </Grid>
        </BoxWrap>

        <BoxWrap display="flex">
          <Grid container spacing={2} pb={2}>
            <Grid item md={6} xs={12} textAlign="right">
              <Typography variant="h6" mb={1}>
                کد ملی
              </Typography>

              <Typography variant="h5" fontWeight={700}>
                {PersianNum(informationUser?.NationalCode)}
              </Typography>
            </Grid>

            <Grid item md={6} xs={12} textAlign="right">
              <Typography variant="h6" mb={1}>
                {" "}
                بابت
              </Typography>

              <Typography variant="h5" fontWeight={700}>
                {selectCurrencyCategory(informationUser?.CurrencyCategory)}
              </Typography>
            </Grid>
          </Grid>
        </BoxWrap>

        <BoxWrap display="flex">
          <Grid container spacing={2} pb={2}>
            <Grid item xs={12} textAlign="right">
              <Typography variant="h6" mb={1}>
                شماره حساب ارزی{" "}
              </Typography>

              <Typography variant="h5" fontWeight={700}>
                {PersianNum(informationUser?.ForeignCurrency)}
              </Typography>
            </Grid>
          </Grid>
        </BoxWrap>

        <BoxWrap display="flex">
          <Grid container spacing={2} pb={2}>
            <Grid item xs={12} md={6}>
              <Typography variant="h6" mb={1}>
                انتخاب تاریخ مراجعه
              </Typography>

              <Typography variant="h5" fontWeight={700}>
                {selectDateReservation}
              </Typography>
            </Grid>

            <Grid item md={6} xs={12} textAlign="right">
              <Typography variant="h6" mb={1}>
                {" "}
                انتخاب ساعت مراجعه
              </Typography>

              <Typography variant="h5" fontWeight={700}>
                {PersianNum(formattedTime)}
              </Typography>
            </Grid>
          </Grid>
        </BoxWrap>

        <Grid container spacing={2} pb={2}>
          <Grid item xs={12}>
            <BoxAlert>
              <Typography
                variant="h6"
                textAlign="center"
                mb={1}
                color="primary.secondary"
              >
                * داشتن کارت ملی به هنگام مراجعه حضوری الزامی است.
              </Typography>
            </BoxAlert>
          </Grid>
        </Grid>

        <Grid container spacing={2} pb={2}>
          <Grid item xs={6}>
            <ButtonStyle fullWidth onClick={handleOnClose}>
              انصراف و بستن
            </ButtonStyle>
          </Grid>

          <Grid item xs={6}>
            <LoadingButton
              variant="contained"
              fullWidth
              color="success"
              onClick={() => dispatch(openModal("formModal"))}
            >
              ویرایش درخواست
            </LoadingButton>
          </Grid>
        </Grid>
      </Box>
    </>
  );
}

const ButtonStyle = styled(Button)(({ theme }) => ({
  background: theme.palette.common.white,
  color: theme.palette.success.main,
}));
const BoxWrap = styled(Box)(({ theme }) => ({
  display: "flex",
  direction: "rtl",
  borderBottom: "1px solid #F0F0F0",
  marginBottom: theme.spacing(2),

  ".MuiBox-root": {
    flexBasis: "50%",
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    paddingBottom: theme.spacing(2),
  },
}));

const BoxAlert = styled(Box)(({ theme }) => ({
  display: "flex",
  direction: "rtl",
  background: "#F6F1E2",
  marginBottom: theme.spacing(2),
  direction: "rtl",
  padding: theme.spacing(1),
}));
