import { Alert, styled, TextField, Typography } from "@mui/material";
import { Box } from "@mui/system";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { LoadingButton } from "@mui/lab";

import MobileIcon from "../../assets/images/PhoneIcon.png";
import { openModal } from "../../store/step";
import http from "../../config/http";
import {
  CreateActivationCodeByMobileNumber,
  GetActiveReserveByMobile,
} from "../../store/api/login";
import {
  setActiveReserve,
  setCodeOtp,
  setMobileNumber,
} from "../../store/login";
import { setInformationUser } from "../../store/informationUser";

export default function AuthModal() {
  const dispatch = useDispatch();
  const mobileNumber = useSelector((state) => state.login.mobileNumber);

  const [inputError, setInputError] = useState("");
  const [loading, setLoading] = useState(false);
  const [notificationError, setNotificationError] = useState(false);

  const getCreateActivationCodeByMobileNumber = async () => {
    try {
      setLoading(true);
      const { data } = await http.get(
        CreateActivationCodeByMobileNumber({ mobileNumber })
      );
      dispatch(setCodeOtp(data.Result));
      dispatch(openModal("otp"));
    } catch (e) {
      setNotificationError(e?.ErrorMessages[0]);
    }
    setLoading(false);
    getActiveReserveByMobile();
  };

  const handleClickAuth = () => {
    getCreateActivationCodeByMobileNumber();
  };

  const handleValue = (e) => {
    dispatch(setMobileNumber(e.target.value));
    validateInput(e.target.value);
  };

  const handleSubmit = (mobile) => {
    if (mobile.length === 11) {
      handleClickAuth();
    }
  };

  useEffect(() => {
    handleSubmit(mobileNumber);
  }, [mobileNumber]);

  const validateNumberInput = (input) => {
    const regex = /^[0-9\b]+$/;
    return regex.test(input);
  };

  const validateInput = (value) => {
    if (!validateNumberInput(value) && value !== "") {
      setInputError("فقط عدد وارد کنید");
    } else if (value.length < 11 && value !== "") {
      setInputError("شماره تماس نباید کمتر از 11 رقم باشد");
    } else if (value.length > 11 && value !== "") {
      setInputError("شماره تماس نباید بیشتر از 11 رقم باشد");
    } else {
      setInputError("");
    }
  };

  const getActiveReserveByMobile = async () => {
    try {
      const { data } = await http.get(
        GetActiveReserveByMobile({ mobileNumber })
      );
      dispatch(setActiveReserve(data?.ResultFlag));
      dispatch(setInformationUser(data?.Result));
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <>
      {notificationError && (
        <Alert
          severity="error"
          sx={{
            direction: "rtl",
            width: "100%",
            position: "relative",
            zIndex: 10,
            top: "32px",
            ".MuiAlert-icon": {
              marginLeft: "5px",
            },
          }}
        >
          {notificationError}
        </Alert>
      )}

      <BoxIcon>
        <Box>
          <img src={MobileIcon} alt="" />
        </Box>
      </BoxIcon>

      <Typography variant="h5" fontWeight={700} textAlign="center" mb={1}>
        شماره موبایل خود را وارد کنید
      </Typography>

      <Typography variant="h6" textAlign="center" mb={3} color="text.secondary">
        اگر سجامی هستید، شماره موبایل سجامی خود را وارد نمایید
      </Typography>

      <TextField
        placeholder="شماره موبایل با 09"
        rowsMin={1}
        sx={{ marginBottom: "24px" }}
        onChange={handleValue}
        error={!!inputError}
        helperText={inputError}
      />

      <LoadingButton
        onClick={handleClickAuth}
        variant="contained"
        fullWidth
        loading={loading}
        color="success"
      >
        دریافت کد
      </LoadingButton>
    </>
  );
}

const BoxIcon = styled(Box)(({ theme }) => ({
  display: "flex",
  justifyContent: "center",
  paddingTop: "40px",
  paddingBottom: "24px",
  ".MuiBox-root ": {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "flex-start",
    padding: "15px",
    gap: "10px",
    width: "88px",
    height: "88px",
    background: "#F8F8F8",
    borderRadius: "50%",
  },
}));
