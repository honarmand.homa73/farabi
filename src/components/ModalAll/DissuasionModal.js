import { Box, Button, styled, Typography, useTheme } from "@mui/material";
import { Trash } from "../../assets/Icon";
import { openModal } from "../../store/step";
import { useDispatch, useSelector } from "react-redux";
import { GetActiveReserveByMobile } from "../../store/api/login";
import { useEffect, useMemo, useState } from "react";
import http from "../../config/http";
import { CancelReservation } from "../../store/api/createReservation";
import { PersianNum } from "../../utils/helpers/PersianNum";
import { selectCurrencyCategory } from "./constance";
import { setMobileNumber } from "../../store/login";

export default function DissuasionModal() {
  // const [informationUser, setInformationUser] = useState({});

  // const mobileNumber = useSelector((state) => state.login.mobileNumber);

  const theme = useTheme();
  const dispatch = useDispatch();
  const informationUser = useSelector(
    (state) => state.informationUser.information
  );

  const selectDateReservation = useMemo(() => {
    if (informationUser?.From !== undefined) {
      return new Date(informationUser?.From)?.toLocaleDateString("fa-IR");
    } else {
      return "";
    }
  }, [informationUser]);

  const formattedTime = useMemo(() => {
    if (informationUser?.From !== undefined) {
      return `${new Date(informationUser?.From)
        ?.toISOString()
        ?.split("T")[1]
        ?.split(".")[0]
        .split(":", 2)
        .join(":")} تا ${new Date(informationUser?.To)
        ?.toISOString()
        ?.split("T")[1]
        ?.split(".")[0]
        .split(":", 2)
        .join(":")}`;
    } else {
      return "";
    }
  }, [informationUser]);
  const idReservation = informationUser?.ID;

  const cancelReservation = async () => {
    try {
      await http.get(CancelReservation({ idReservation }));
      dispatch(openModal("dissuasion"));
      dispatch(setMobileNumber(""));
    } catch (e) {
      console.log(e.ErrorMessages);
    }
  };

  // useEffect(() => {
  //   getActiveReserveByMobile();
  // }, []);

  return (
    <>
      <BoxIcon>
        <Box>
          <Trash color={theme.palette.error.main} size={60} />
        </Box>
      </BoxIcon>

      <Typography
        variant="h5"
        fontWeight={700}
        textAlign="center"
        mb={5}
        dir="rtl"
      >
        آیا از لغو درخواست زیر اطمینان دارید؟
      </Typography>

      <BoxWrap>
        <Box>
          <Typography
            variant="h6"
            textAlign="center"
            mb={1}
            color="text.secondary"
          >
            نام
          </Typography>
          <Typography variant="h5" fontWeight={700} textAlign="center" mb={1}>
            {informationUser?.FirstName}
          </Typography>
        </Box>

        <Box>
          <Typography
            variant="h6"
            textAlign="center"
            mb={1}
            color="text.secondary"
          >
            نام خانوادگی
          </Typography>
          <Typography variant="h5" fontWeight={700} textAlign="center" mb={1}>
            {informationUser?.LastName}
          </Typography>
        </Box>
      </BoxWrap>

      <BoxWrap>
        <Box>
          <Typography
            variant="h6"
            textAlign="center"
            mb={1}
            color="text.secondary"
          >
            کد ملی
          </Typography>
          <Typography variant="h5" fontWeight={700} textAlign="center" mb={1}>
            {PersianNum(informationUser?.NationalCode)}
          </Typography>
        </Box>

        <Box>
          <Typography
            variant="h6"
            textAlign="center"
            mb={1}
            color="text.secondary"
          >
            بابت
          </Typography>
          <Typography variant="h5" fontWeight={700} textAlign="center" mb={1}>
            {selectCurrencyCategory(informationUser?.CurrencyCategory)}
          </Typography>
        </Box>
      </BoxWrap>

      <BoxWrap display="flex">
        <Box>
          <Typography variant="h6" mb={1}>
            شماره حساب ارزی{" "}
          </Typography>

          <Typography variant="h5" fontWeight={700}>
            {PersianNum(informationUser?.ForeignCurrency)}
          </Typography>
        </Box>
      </BoxWrap>

      <BoxWrap>
        <Box>
          <Typography
            variant="h6"
            textAlign="center"
            mb={1}
            color="text.secondary"
          >
            تاریخ مراجعه
          </Typography>
          <Typography variant="h5" fontWeight={700} textAlign="center" mb={1}>
            {selectDateReservation}
          </Typography>
        </Box>

        <Box>
          <Typography
            variant="h6"
            textAlign="center"
            mb={1}
            color="text.secondary"
          >
            ساعت مراجعه
          </Typography>
          <Typography variant="h5" fontWeight={700} textAlign="center" mb={1}>
            {PersianNum(formattedTime)}
          </Typography>
        </Box>
      </BoxWrap>

      <Box display="flex" dir="rtl">
        <Button
          fullWidth
          color="text"
          onClick={() => dispatch(openModal("formModal"))}
        >
          {" "}
          انصراف و بازگشت
        </Button>
        <Button fullWidth color="error" onClick={cancelReservation}>
          بله حذف شود{" "}
        </Button>
      </Box>
    </>
  );
}

const BoxIcon = styled(Box)(({ theme }) => ({
  display: "flex",
  justifyContent: "center",
  paddingTop: "20px",
  paddingBottom: "24px",
  ".MuiBox-root ": {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "flex-start",
    padding: "15px",
    gap: "10px",
    width: "88px",
    height: "88px",
    background: "#FDEFED",
    borderRadius: "50%",
  },
}));

const BoxWrap = styled(Box)(({ theme }) => ({
  display: "flex",
  direction: "rtl",
  borderBottom: "1px solid #F0F0F0",
  marginBottom: theme.spacing(2),

  ".MuiBox-root": {
    flexBasis: "50%",
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    paddingBottom: theme.spacing(2),
  },
}));
