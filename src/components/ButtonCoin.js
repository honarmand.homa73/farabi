import { Button, styled } from "@mui/material";
import { useDispatch } from "react-redux";
import { openModal } from "../store/step";
import { setMobileNumber } from "../store/login";
import { setSelectedCoin } from "../store/selectCoin";
export default function ButtonCoin() {
  const dispatch = useDispatch();

  const handleClick = () => {
    dispatch(setMobileNumber(""));
    dispatch(setSelectedCoin(""));

    dispatch(openModal("login"));
  };
  return (
    <ButtonStyle variant="text" onClick={handleClick}>
      ثبت/ ویرایش نوبت دهی خرید ارز
    </ButtonStyle>
  );
}

const ButtonStyle = styled(Button)(({ theme }) => ({
  alignItems: "center",
  padding: "0px 20px",
  margin: "0 !important",
  height: "40px",
  background: theme.palette.background.button,
  color: theme.palette.text.main,
  borderRadius: "6px",
  flex: "none",
  order: "0",
  alignSelf: "stretch",
  flexGrow: "0",
  [theme.breakpoints.down("sm")]: {
    width: "100%",
  },
  "&:hover": {
    background: theme.palette.background.button,
  },
}));
