import {
  LocalizationProvider,
  MobileDatePicker,
} from "@mui/x-date-pickers-pro";
import { TextField, useTheme } from "@mui/material";
import { AdapterDateFnsJalali } from "@mui/x-date-pickers/AdapterDateFnsJalali";

function DatePicker({ value, onChange, enabledDates }) {
  const theme = useTheme();

  const isDateEnabled = (date) => {
    // Custom logic to determine if a date should be enabled
    return enabledDates.some(
      (enabledDate) =>
        date.toISOString().split("T")[0] ===
        enabledDate.toISOString().split("T")[0]
    );
  };
  const customPtBRLocaleText = {
    okButtonLabel: "تأیید",
    cancelButtonLabel: "لغو",
  };

  return (
    <LocalizationProvider
      dateAdapter={AdapterDateFnsJalali}
      localeText={customPtBRLocaleText}
    >
      <MobileDatePicker
        renderInput={(params) => (
          <TextField placeholder="YYYY/MM/DD" {...params} />
        )}
        DialogProps={{
          PaperProps: {
            sx: {
              ".MuiPickersCalendarHeader-label": {
                fontFamily: " IRANSansNum !important",
              },
              ".MuiDayPicker-header ": {
                ".MuiTypography-root": {
                  fontFamily: " IRANSansNum !important",
                },
              },
              ".MuiButtonBase-root": {
                color: theme.palette.text.primary,
                fontFamily: " IRANSansNum !important",

                "&.Mui-selected": {
                  backgroundColor: theme.palette.primary.main,
                  color: theme.palette.text.main,
                },
              },
              ".MuiDialogActions-root": {
                ".MuiButtonBase-root": {
                  color: theme.palette.common.white,
                },
              },
            },
          },
        }}
        showToolbar={false}
        value={value}
        closeOnSelect
        onChange={onChange}
        shouldDisableDate={(date) => !isDateEnabled(date)}
      />
    </LocalizationProvider>
  );
}

export default DatePicker;
